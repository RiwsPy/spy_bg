#!/usr/bin/python
import re

from common import Command as CommandBase
from models import Item, Spell
from utils import SchoolChoices, SpellTypeChoices
from utils.enums import Duration


class Command(CommandBase):
    help = "Log les erreurs des sorts"
    _queryset = None
    _scrolls = None
    fields = (
        "cartridge",
        "name",
        "school",
        "level",
        "range",
        "duration",
        "casting_time",
        "effect_zone",
        "saving_throw",
        # "dispel",
        # "resist_magic",
    )

    def get_queryset(self) -> list:
        if self._queryset:
            return self._queryset

        def is_valid(spl):
            return (
                spl.description
                and spl.description.entry > 0
                and spl.name
                and spl.name.entry > 0
                and spl.signature.startswith("SPL")
            )

        self._queryset = [
            spl
            for file in self.files
            if file.endswith(".spl")
            and is_valid(spl := Spell.create_from_filename(file))
        ]
        return self._queryset + self.get_scroll_queryset()

    def get_scroll_queryset(self) -> list:
        if self._scrolls:
            return self._scrolls

        def is_valid(itm):
            return (
                itm.description
                and itm.description.entry > 0
                and itm.name
                and itm.name.entry > 0
                and itm.signature.startswith("ITM")
                and itm.type == 11  # Scroll
                and itm.extended_count >= 1
                and itm.headers[0].effects
                and itm.headers[0].effects[0].opcode_number in (146, 148)
            )

        self._scrolls = [
            itm
            for file in self.files
            if file.endswith(".itm")
            and is_valid(itm := Item.create_from_filename(file))
        ]
        return self._scrolls

    def is_playable(self, spell) -> bool:
        # TODO: à terminer
        filename = spell.filename.upper()
        if len(filename) == 8 and filename.endswith(("D", "P", "F")):
            return False
        if filename.startswith("SPWISH"):
            return False
        prefix = filename[:4]
        if prefix in ("SPCL", "SPDR", "SPRA", "SPWM", "SPJA"):
            return True
        if prefix == "SPPR" and int(filename[4]) < 8:
            return True
        if prefix == "SPWI" and int(filename[4]) > 1 and int(filename[5:7]) < 40:
            return True
        if prefix == "SPIN" and int(filename[4]) < 2:
            return True
        return False

    def _get_spell(self, obj):
        if isinstance(obj, Item):
            resource = obj.headers[0].effects[0].resource1
            if resource is None:
                return None
            spell = Spell.create_from_filename(resource)
        else:
            spell = obj
        return spell

    def handle(self, *args, **kwargs) -> None:
        super().handle(*args, **kwargs)
        regex_level = re.compile(r"Niveau : (?P<level>\w+)\n")

        for obj in self.get_queryset():
            spell = self._get_spell(obj)
            if spell is None:
                continue

            try:
                desc_name, desc_school, description = obj.description.split("\n", 2)
            except ValueError:
                description = ""
                try:
                    desc_name, desc_school = obj.description.split("\n", 1)
                except ValueError:
                    desc_name = obj.description
                    desc_school = ""

            # check name/description
            if "name" in self.fields and desc_name != obj.name:
                self._add_error(
                    obj,
                    "Nom",
                    f"{obj.name} ({obj.name.entry})",
                    f"{desc_name} ({obj.description.entry})",
                )

            # check school
            if "school" in self.fields:
                if not desc_school.startswith("(") or not desc_school.endswith(")"):
                    if spell.school != SchoolChoices.ALL:
                        self._add_error(obj, "École de magie", spell.school.label)
                elif not desc_school.startswith(f"({spell.school.label}"):
                    self._add_error(
                        obj, "École de magie", spell.school.label, desc_school
                    )

            # FIXME: insuffisant
            if "cartridge" in self.fields and obj.description.count(" :") < 2:
                self._add_error(obj, "Cartouche absente", obj.description)

            # level check
            if (
                "level" in self.fields
                and spell.level > 0
                and spell.type
                in (
                    SpellTypeChoices.WIZARD,
                    SpellTypeChoices.PRIEST,
                )
            ):
                toto = regex_level.search(description)
                if not toto:
                    self._add_error(obj, "Niveau", spell.level)
                else:
                    try:
                        level = int(toto.group("level"))
                    except ValueError:
                        level = toto.group("level")
                    if spell.level != level:
                        self._add_error(obj, "Niveau", spell.level, level)

        # range check
        if "range" in self.fields:
            self._effects_check("Portée", "range", self._check_range)

        # duration check
        if "duration" in self.fields:
            self._effects_check("Durée", None, self._check_duration)

        # casting_time check
        if "casting_time" in self.fields:
            self._effects_check(
                "Temps d'incantation", "casting_time", self._check_casting_time
            )

        # effect_zone check
        if "effect_zone" in self.fields:
            desc_name = "Zone d'effet"

            regex = re.compile(f"\n{desc_name}" + r" : (?P<toto>.+)\n")
            for obj in self.get_queryset():
                spell = self._get_spell(obj)
                if spell is None:
                    continue

                value = self._check_zone(spell)
                if value is None:
                    continue

                toto = regex.search(obj.description)
                value_desc = None
                if toto is not None:
                    value_desc = toto.group("toto")

                if value == value_desc:
                    continue
                self._add_error(obj, desc_name, value, value_desc)

        # js check
        if "saving_throw" in self.fields:
            self._effects_check("Jet de sauvegarde", None, self._check_st)

        # dissipation check
        if "dispel" in self.fields:
            self._effects_check("Dissipation", None, self._check_dissipable)

        # résistance à la magie check
        if "resist_magic" in self.fields:
            self._effects_check("Résistance à la magie", None, self._check_resist_magic)

    def _add_error(self, obj, desc_name, expected, value=None) -> None:
        space_nb = 30 - len(desc_name)
        msg = f"{desc_name} : {space_nb * ' '} {value or 'non renseigné'} ⇒ {expected}"
        entry = f"{obj.filename} ({obj.name or 'inconnu' }) ⇒ Strref {obj.description.entry}"

        self.filename_info[entry].append(msg)

    def _effects_check(self, desc_name: str, field_name: str | None, method) -> None:
        regex = re.compile(f"{desc_name}" + r" : (?P<foo>.+)\n")
        for obj in self.get_queryset():
            spell = self._get_spell(obj)
            if spell is None:
                continue

            match = regex.search(obj.description)
            if field_name:
                values = set(getattr(eff, field_name) for eff in spell.headers)
                if len(values) > 1:
                    self._add_error(obj, f"{desc_name}", values)
                    continue

            desc_value = None
            if match is not None:
                desc_value = match.group("foo")
            expected_value = method(spell)

            if expected_value != desc_value:
                self._add_error(obj, desc_name, expected_value, desc_value)

    def _check_range(self, spell) -> str:
        return spell.get_range_desc()

    def _check_casting_time(self, spell) -> str:
        return f"{spell.casting_time}"

    def _check_zone(self, spell):
        return spell.get_zone_desc()

    def _check_st(self, spell):
        return spell.get_saving_throws_desc()

    def _check_duration(self, spell):
        return spell.get_duration_desc()

    def _check_dissipable(self, spell):
        if (
            is_dissipable := spell.is_dissipable
        ) is not True and spell.get_duration_desc() != Duration.INSTANT.value:
            return {False: "non-affecté", None: "spécial"}.get(is_dissipable)

    def _check_resist_magic(self, spell):
        if spell.is_hostile:
            return {False: "non-affecté", None: "spécial"}.get(spell.is_resist_magic)


if __name__ == "__main__":
    Command()
