#!/usr/bin/python
import re
from collections import defaultdict

# from models.creature import Creature
from files import GameFiles
from models.spell import Spell
from utils.strref import DialogBase

if __name__ == "__main__":
    import time

    print("starting")
    start = time.time()

    data = dict()
    for game in ("BGEE-SOD", "BG2EE"):
        qs = dict()
        files = GameFiles(game=game)
        DialogBase(game=game)
        for file, path in files._data.items():
            if not file.endswith(".spl"):
                continue
            spl = Spell.create_from_path(path)
            if (spl.description and spl.description.entry > 0) and (
                spl.name and spl.name.entry > 0
            ):
                qs[file.lower()] = [
                    spl,
                    spl.name,
                    spl.description,
                    spl.description.entry if spl.description is not None else 0,
                ]
        data[game] = qs

    common_spells = data["BGEE-SOD"].keys() & data["BG2EE"].keys()
    errors = defaultdict(list)

    def diff_desc(v1, v2, entry1, entry2, desc_name=""):
        if v1 != v2 and v1 is not None and v2 is not None and entry1 and entry2:
            diff = len(v1) - len(v2)
            base = f"{entry1} ⇒ {entry2}"
            errors[filename].append(
                f"{base} ; {desc_name} différente : {diff} caractères"
            )

            for line1, line2 in zip(v1.split("\n"), v2.split("\n")):
                if line1 != line2:
                    errors[filename].append(f"diff :\n\t\t{line1}\n\t\t{line2}")

    cartouche_end = re.compile(r"Jet de sauvegarde : .*")

    for filename in common_spells:
        ee_name, ee_description, ee_entry = data["BGEE-SOD"][filename][1:]
        e2_name, e2_description, e2_entry = data["BG2EE"][filename][1:]
        if ee_name != e2_name:
            errors[filename].append(f"Nom différent : {ee_name} ⇒ {e2_name}")

        try:
            ee_cartouche, ee_desc = cartouche_end.split(ee_description)
            e2_cartouche, e2_desc = cartouche_end.split(e2_description)
        except ValueError:
            diff_desc(ee_description, e2_description, ee_entry, e2_entry, "Description")
        else:
            diff_desc(ee_cartouche, e2_cartouche, ee_entry, e2_entry, "Cartouche")
            diff_desc(ee_desc, e2_desc, ee_entry, e2_entry, "Description")

    with open("src/logs/BGXEE_spells_diff.log", "w+") as f:
        txt = ""
        for filename, errors in sorted(errors.items()):
            txt += f"{filename}\n\t" + "\n\t".join(errors) + "\n"
        f.write(txt)

    print("finished")
    print(time.time() - start)
