import inspect
import sys
from typing import Self

from files import da, ids
from utils.enums import SchoolChoices
from utils.enums.ids_reverse import State


class StatMixin:
    initial = 0

    def __init__(self, parent, initial=None):
        self.parent = parent
        if initial is not None:
            self.initial = initial
        self.mod = 0
        self.permanent = initial
        self.set = None

    @property
    def cre(self):
        return self.parent.parent

    @property
    def value(self) -> int:
        if self.set is not None:
            value = self.set
        elif self.permanent is not None:
            value = self.permanent
        else:
            value = self.initial

        return value + self.mod

    @staticmethod
    def from_name(stat_name: str) -> Self:
        return stat_mapping.get(stat_name, StatMixin)

    def __iadd__(self, other):
        self.mod += other
        return self

    def __isub__(self, other):
        self.mod -= other
        return self

    def __getattr__(self, stat_name):
        if stat_name.isupper():
            return self.parent[stat_name].value
        raise AttributeError


class LevelMixin(StatMixin):
    @property
    def value(self):
        return max(min(50, super().value), 0)


class LEVEL(LevelMixin):
    pass


class LEVEL2(LevelMixin):
    pass


class LEVEL3(LevelMixin):
    pass


class HITPOINTS(StatMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set = self.initial

    @property
    def value(self):
        # borné en [-32768, 32767]
        return max(self.set, self.MINHITPOINTS)


class EXP(StatMixin):
    @property
    def value(self):
        # TODO: l'information est probablement pas signée
        value = super().value
        if value < 0:
            value = self.parent.exp_max
        return min(value, self.parent.exp_max)


class LUCK(StatMixin):
    @property
    def value(self):
        fatig_mod = int(da.FATIGMOD[self.FATIGUE].LUCK)
        intox_mod = int(da.INTOXMOD[self.INTOXICATION].LUCK)
        return super().value + fatig_mod + intox_mod


class FATIGUE(StatMixin):
    @property
    def value(self):
        return max(min(100, super().value), 0)


class INTOXICATION(StatMixin):
    @property
    def value(self):
        return max(min(100, super().value), 0)


class MORALE(StatMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set = self.initial

    @property
    def value(self):
        intox_mod = int(da.INTOXMOD[self.INTOXICATION].MORALE)
        return self.set + intox_mod


class CaracMixin(StatMixin):
    @property
    def value(self):
        return min(25, super().value)


class STR(CaracMixin):
    pass


class DEX(CaracMixin):
    pass


class CON(CaracMixin):
    pass


class INT(CaracMixin):
    pass


class WIS(CaracMixin):
    pass


class CHR(CaracMixin):
    pass


class THAC0(StatMixin):
    @property
    def value(self):
        value = super().value
        if self.cre.state & State.STATE_BERSERK:
            value -= 2
        return value


class DAMAGEBONUS(StatMixin):
    @property
    def value(self):
        value = super().value
        if self.cre.state & State.STATE_BERSERK:
            value += 2
        return value


class VISUALRANGE(StatMixin):
    @property
    def value(self):
        if self.cre.state & State.STATE_BLIND:
            return 2
        return super().value


class MAXHITPOINTS(StatMixin):
    @property
    def value(self):
        return max(super().value, self.cre.level_average)


class MORALERECOVERYTIME(StatMixin):
    @property
    def value(self):
        return max(1, super().value)


class SchoolSaveMixin(StatMixin):
    @property
    def value(self):
        value = super().value
        school = self.cre.kit.mage_school
        if school and school.name == getattr(SchoolChoices, self.__class__.__name__):
            value -= 2
        return value


class ABJURATION(SchoolSaveMixin):
    pass


class CONJURATION(SchoolSaveMixin):
    pass


class DIVINATION(SchoolSaveMixin):
    pass


class ENCHANTMENT(SchoolSaveMixin):
    pass


class ILLUSION(SchoolSaveMixin):
    pass


class INVOCATION(SchoolSaveMixin):
    pass


class NECROMANCY(SchoolSaveMixin):
    pass


class TRANSMUTER(SchoolSaveMixin):
    pass


class ACCRUSHINGMOD(StatMixin):
    pass


class ACMISSILEMOD(StatMixin):
    pass


class ACPIERCINGMOD(StatMixin):
    pass


class ACSLASHINGMOD(StatMixin):
    pass


stat_class_names = set(stat_name for stat_name in ids.STATS.values())
# Création dynamique des classes restantes
for class_name in stat_class_names - globals().keys():
    globals()[class_name] = type(class_name, (StatMixin,), {})

# Génère automatiquement le dictionnaire des stats : {"STAT": statClass}
stat_mapping = dict(
    (k, v)
    for k, v in inspect.getmembers(sys.modules[__name__], inspect.isclass)
    if issubclass(v, StatMixin)
)
