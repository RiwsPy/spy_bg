from models.effect import EffectV1
from utils import BaseType, Byte, Char, Dword, Resref, StrrefPK, Word

from .common import BgBase, HeaderBase


class Header(HeaderBase):
    fields = {
        "attack_type": Byte(),
        "id_required": Byte(),
        "location": Byte(),
        "alternative_dice_sides": Byte(),
        "icon": Resref(),
        "target": Byte(),
        "target_count": Byte(),  # ce qui permet de viser plusieurs cibles avec la même charge
        "_range": Word(),
        "launcher": Byte(),
        "alternative_dice_thrown": Byte(),
        "speed_factor": Byte(),
        "alternative_damage_bonus": Byte(),
        "thac0_bonus": Word(signed=True),
        "dice_sides": Byte(),
        "primary_school": Byte(),
        "dice_thrown": Byte(),
        "secondary_school": Byte(),
        "damage_bonus": Word(signed=True),
        "damage_type": Word(),
        "feature_count": Word(),
        "feature_offset": Word(),
        "max_charges": Word(),
        "charge_depletion": Word(),
        "flags": Dword(),
        "_projectile": Word(),
        "animation": Word(size=12, klass=None),
    }


class Item(BgBase):
    ext = ".itm"
    expected_signature = "ITM "
    fields = {
        "signature": Char(),
        "version": Char(),
        "uname": StrrefPK(),
        "name": StrrefPK(),
        "replacement_item": Resref(),
        "flags": Dword(),
        "type": Word(),
        "usability": Dword(),
        "animation": Char(size=2),
        "level_min": Word(),
        "strength_min": Word(),
        "strength_bonus_min": Byte(),
        "kit_usability1": Byte(),
        "intelligence_min": Byte(),
        "kit_usability2": Byte(),
        "dexterity_min": Byte(),
        "kit_usability3": Byte(),
        "wisdom_min": Byte(),
        "kit_usability4": Byte(),
        "constitution_min": Byte(),
        "proficiency": Byte(),
        "charisma_min": Word(),
        "price": Dword(),
        "stack_amount": Word(),
        "inventory_icon": Resref(),
        "lore": Word(),
        "ground_icon": Resref(),
        "weight": Dword(),
        "udescription": StrrefPK(),
        "description": StrrefPK(),
        "description_icon": Resref(),
        "enchantment": Dword(),
        "extended_offset": Dword(),
        "extended_count": Word(),
        "blocks_offset": Dword(),
        "equip_offset": Word(),
        "equip_count": Word(),
        "headers": BaseType(
            klass=list,
            model=Header,
            nb_key="extended_count",
            sub=BaseType(
                klass=list, model=EffectV1, nb_key="feature_count", name="_effects"
            ),
        ),
        "equip_effects": BaseType(klass=list, model=EffectV1, nb_key="equip_count"),
    }
