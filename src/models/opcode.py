import inspect
import sys
from enum import IntFlag
from functools import cached_property
from typing import Self

from files import da, ids
from utils import TimingOpcode
from utils.dice import Dice
from utils.enums import (
    JSChoices,
    JSImpactDisplay,
    SchoolChoices,
    TargetType,
    TimeUnits,
    TimingModes,
    ZoneDesc,
    st_stat_name,
)
from utils.enums.creature import TypeBase
from utils.enums.ids_reverse import Dmgtype, State
from utils.tra import translate_creature_type

from ._opcode_list import (
    CAN_BE_INSTANT_OPCODES,
    INSTANT_OPCODES,
    IS_SAVABLE_OPCODES,
    NOT_PERMANENT_AFTER_DEATH_OPCODES,
    OPCODES,
    PERMANENT_OPCODES,
    USED_OPCODES,
)


def opcode_classname(opcode_number: int) -> str:
    return f"Opcode{opcode_number}"


def split_32bit(value: int) -> tuple[int, int]:
    return divmod(value, 0x10000)


def signed_byte_value(value: int, byte_count: int) -> int:
    """
    Interprète un entier comme un nombre signé de byte_count octets.

    Les valeurs sont comprises entre 0 et 2^byte_count-1.
    Les valeurs supérieures à 2^(byte_count-1) sont considérées comme négatives.
    """
    value &= (1 << byte_count) - 1
    if value > (1 << (byte_count - 1)):
        return value - (1 << byte_count)
    return value


def check_match_target(opcode, target, default: bool = False) -> bool:
    # aucune restriction
    if opcode.p1 == 0:
        return True

    match opcode.p2:
        case 2:
            return target.ea == opcode.p1
        case 3:
            return target.general == opcode.p1
        case 4:
            return target.race == opcode.p1
        case 5:
            return target.klass == opcode.p1
        case 6:
            return target.specific == opcode.p1
        case 7:
            return target.gender == opcode.p1
        case 8:
            align = target.align
            if align == opcode.p1:
                return True
            # MASK_GOOD/GENEUTRAL/EVIL
            if 1 <= opcode.p1 <= 3:
                return align % 10 == opcode.p1
            # MASK_LAWFUL/LCNEUTRAL/CHAOTIC
            if 1 <= opcode.p1 / 10 <= 3:
                return align // 10 * 10 == opcode.p1
            return False
        case 9:
            return target.kit == opcode.p1

    return default


class OpcodeMeta(type):
    def __new__(cls, name, bases, attrs, **kwargs):
        new_class = super().__new__(cls, name, bases, attrs, **kwargs)

        opcode_number = getattr(new_class, "opcode_number", -1)
        if opcode_number == -1:
            try:
                opcode_number = int(name.removeprefix("Opcode"))
            except ValueError:
                return new_class

        new_class.opcode_number = opcode_number

        attr_mapping = {
            "_is_used": USED_OPCODES,
            "_is_instant": INSTANT_OPCODES,
            "_is_can_be_instant": CAN_BE_INSTANT_OPCODES,
            "_is_permanent": PERMANENT_OPCODES,
            "_is_not_permanent_after_death": NOT_PERMANENT_AFTER_DEATH_OPCODES,
            "_is_savable": IS_SAVABLE_OPCODES,
        }

        for attr, qs in attr_mapping.items():
            if getattr(new_class, attr, None) is None:
                setattr(new_class, attr, opcode_number in qs)

        return new_class


class OpcodeBase(metaclass=OpcodeMeta):
    min_value: int | None = None
    max_value: int | None = None
    mult: int = 1  # sur certains opcodes, le +X fait un -X (pour les JS entre autre)
    first_apply: bool = True

    def __init__(self, effect):
        self.effect = effect

    @staticmethod
    def from_int(opcode_number: int) -> Self:
        return opcode_mapping.get(opcode_classname(opcode_number), OpcodeBase)

    @cached_property
    def timing(self) -> TimingOpcode:
        return TimingOpcode(self)

    @cached_property
    def children(self) -> tuple:
        return tuple()

    @cached_property
    def p1(self) -> int:
        return self.effect.p1

    @cached_property
    def p2(self) -> int:
        return self.effect.p2

    @cached_property
    def p3(self) -> int:
        return self.effect.p3

    @cached_property
    def p4(self) -> int:
        return self.effect.p4

    @cached_property
    def p5(self) -> int:
        return self.effect.p5

    @cached_property
    def dispel_and_resistance(self) -> int:
        return self.effect.dispel_and_resistance

    @cached_property
    def timing_mode(self) -> int:
        return self.effect.timing_mode

    @cached_property
    def duration(self) -> int:
        return self.effect.duration

    @cached_property
    def special(self) -> int:
        return self.effect.special

    @cached_property
    def dice_thrown(self) -> int:
        return self.effect.dice_thrown

    @cached_property
    def dice_sides(self) -> int:
        return self.effect.dice_sides

    @cached_property
    def level_min(self) -> int:
        return self.effect.level_min

    @cached_property
    def level_max(self) -> int:
        return self.effect.level_max

    @cached_property
    def resource1(self) -> str:
        return self.effect.resource1

    @cached_property
    def resource2(self) -> str:
        return self.effect.resource2

    @cached_property
    def resource3(self) -> str:
        return self.effect.resource3

    @cached_property
    def saving_throw(self) -> int:
        return self.effect.saving_throw

    @cached_property
    def saving_throw_bonus(self) -> int:
        return self.effect.saving_throw_bonus

    @cached_property
    def primary_school(self) -> int:
        return SchoolChoices(self.effect.primary_school)

    @cached_property
    def probability_start(self):
        return self.effect.probability_start

    @cached_property
    def probability_end(self):
        return self.effect.probability_end

    def is_valid(self) -> bool:
        return True

    # TODO
    # def _can_apply(self, target) -> bool:
    #     if self.effect.is_resist_magic:
    #         pass

    @cached_property
    def stat_name(self) -> str:
        return ""

    def apply(self, source, target=None, *, apply: bool = True) -> None:
        self.source = source
        self.target = target or source
        if not self.is_valid() or self.opcode_number in self.target.immune_opcodes:
            return

        if self.is_savable:
            self.save()
        if apply:
            with self.target.alter_effects():
                self.apply_on()
                self.first_apply = False

    def apply_on(self) -> None:
        """
        Applique les effets sans conditions
        Peut être réappliqué à l'avenir
        """
        pass

    def save(self) -> None:
        """
        Sauvegarde l'effet sur la cible
        """
        if self.timing_mode.is_equiped:
            self.target.equiped_effects.append(self)
        else:
            self.target.current_effects.append(self)

    @cached_property
    def probability(self) -> float:
        if (
            self.probability_start > self.probability_end
            or self.probability_start >= 100
        ):
            return 0.0
        return (min(99, self.probability_end) - self.probability_start + 1) / 100

    @cached_property
    def is_instant(self) -> bool:
        # influe la description de la durée de l'effet
        return self._is_instant or (self.duration <= 1 and self.is_can_be_instant)

    @cached_property
    def is_can_be_instant(self) -> bool:
        return self._is_can_be_instant

    @cached_property
    def is_used(self) -> bool:
        return self._is_used

    @cached_property
    def is_permanent(self) -> bool:
        # influe la description de la durée de l'effet
        return self._is_permanent

    @cached_property
    def is_not_permanent_after_death(self) -> bool:
        return self._is_not_permanent_after_death

    @cached_property
    def is_savable(self) -> bool:
        return self._is_savable

    @cached_property
    def have_dice(self) -> bool:
        # Détermine si les cases dice_throw et dice_size sont utilisées ou non
        # Si oui, pour les effets V1, les cases level_min et level_max ne sont plus utilisables
        return False

    @property
    def dice(self) -> tuple[int, int]:
        return self.dice_thrown, self.dice_sides

    @cached_property
    def is_dissipable(self) -> bool:
        return bool(self.dispel_and_resistance & 0x1)

    @cached_property
    def is_resist_magic(self) -> bool:
        return self.dispel_and_resistance & 0x3 == 1

    def _apply_percent(self, target) -> float:
        # TODO: demi JS de dégâts
        value = self.probability
        if (
            self.level_min > target.level1 > self.level_max or value == 0.0
            # or self.filename in target.immune_items
        ):
            return 0.0

        # RM
        if self.is_resist_magic:
            value *= 1.0 - min(target.stats.RESISTMAGIC.value, 100.0) / 100.0

        # JS
        effect_save = self.saving_throw & JSChoices.ALL
        if not effect_save:
            return value

        # En cas de cumul de JS, seul le meilleur est conservé (= le plus bas)
        st_value = min(
            getattr(target.stats, stat_name).value
            for i, stat_name in enumerate(st_stat_name)
            if effect_save & (1 << i)
        )

        # JS >= 20 : échec
        if st_value >= 20:
            return value

        # FIXME: JS bonus contre une école spécifique
        if self.primary_school:
            st_value -= getattr(target.stats, self.primary_school.stat_name).value

        st_value -= self.saving_throw_bonus
        st_value -= target.save_mod
        value *= min(1.0, max(0.0, st_value / 20))
        # if self.parent and self.parent.ext == self.ext:
        #     value *= self.parent._apply_percent(target)
        return value

    def get_saving_throws_desc(self) -> str:
        current_impact = JSImpactDisplay.NEGATE.value
        if self.saving_throw & JSChoices.ALL == JSChoices.NONE:
            current_impact = JSImpactDisplay.NONE.value
        return current_impact

    def get_zone_desc(self) -> str:
        if self.children and hasattr(self.children[0], "get_zone_desc"):
            # FIXME
            return self.children[0].get_zone_desc()
        return ZoneDesc.NONE

    def get_zone_restriction(self) -> str:
        if self.children and hasattr(self.children[0], "get_zone_restriction"):
            # FIXME
            return self.children[0].get_zone_restriction()
        return TypeBase.NONE


class OpcodeStats(OpcodeBase):
    # Mixin pour opcode qui modifient les caractéristiques
    def is_valid(self) -> bool:
        return 0 <= self.p2 <= 2

    @cached_property
    def p1(self):
        return signed_byte_value(super().p1, 32)

    def apply_on(self) -> None:
        stat_name = self.stat_name
        if self.p1 == 0 and not self.timing.is_permanent:
            self.target.stats[stat_name].mod += self.p1 * self.mult
            return

        match self.p2:
            case 0:
                next_value = (
                    self.target.stats[stat_name].permanent + self.p1 * self.mult
                )
            case 1:
                next_value = self.p1
            case 2:
                next_value = (
                    int(self.p1 / 100 - 1) * self.target.stats[stat_name].permanent
                )
            case _:
                return

        if self.max_value is not None:
            next_value = min(self.max_value, next_value)
        if self.min_value is not None:
            next_value = max(self.min_value, next_value)

        if self.timing.is_permanent:
            self.target.stats[stat_name].permanent = next_value
        else:
            self.target.stats[stat_name].mod = next_value


class OpcodeCarac(OpcodeStats):
    # Mixin pour les 6 caractéristiques de base du personnage (force, dex, …)
    min_value = 0
    max_value = 25
    # TODO: influence de IGNOREDRAINDEATH

    def is_valid(self):
        return self.p2 <= 2


class OpcodeResist(OpcodeStats):
    # n'utilise pas permanent_stats
    min_value = -128
    max_value = 127


class Opcode0(OpcodeBase):
    mult = -1
    min_value = -20
    max_value = 20
    stat_name_map: dict[int, str] = {
        0: "ARMORCLASS",
        1: "ACCRUSHINGMOD",
        2: "ACMISSILEMOD",
        4: "ACPIERCINGMOD",
        8: "ACSLASHINGMOD",
        16: "ARMORCLASS",
    }

    @cached_property
    def p1(self):
        return signed_byte_value(super().p1, 16)

    @cached_property
    def stat_name(self) -> str:
        return self.stat_name_map.get(self.p2, "")

    def is_valid(self) -> bool:
        return super().is_valid() and self.p2 in self.stat_name_map

    def apply_on(self) -> None:
        stat_name = self.stat_name

        if self.timing.is_permanent:
            current_value = self.target.stats[stat_name].permanent
        elif self.p2 == 16:
            current_value = self.target.stats[stat_name].mod
        else:
            self.target.stats[stat_name].mod += self.p1 * self.mult
            return

        if self.p2 != 16:
            next_value = current_value + self.p1 * self.mult
        # n'est prise en compte que si la valeur est inférieure à l'actuelle
        elif self.p1 < current_value:
            next_value = self.p1
        else:
            return

        if self.max_value is not None:
            next_value = min(self.max_value, next_value)
        if self.min_value is not None:
            next_value = max(self.min_value, next_value)

        if self.timing.is_permanent:
            self.target.stats[stat_name].permanent = next_value
        else:
            self.target.stats[stat_name].set = next_value


class Opcode5(OpcodeBase):
    def get_zone_restriction(self):
        if self.p1 > 0 and (restriction := translate_creature_type("GENERAL", self.p1)):
            return restriction
        return super().get_zone_restriction()

    def is_valid(self):
        return self.p1 in (0, self.target.general)

    def apply_on(self):
        self.target.mod_state &= State.STATE_CHARMED
        # TODO: EA


class Opcode6(OpcodeCarac):
    stat_name = "CHR"


class Opcode10(OpcodeCarac):
    stat_name = "CON"


class Opcode12(OpcodeBase):
    have_dice = True

    class SpecialFlags(IntFlag):
        SAVE_FOR_NEGATE = 0x0
        DAMAGE_DRAIN = 0x1
        REVERSE_DAMAGE_DRAIN = 0x2
        FIST_ONLY = 0x4
        DRAIN_DAMAGE_NO_STACK = 0x8
        REVERSE_DAMAGE_DRAIN_NO_STACK = 0x10
        NO_FEEDBACK_STRING = 0x20
        MIN_HP_PROTECTION = 0x40
        ZERO_HP_PROTECTION = 0x80
        SAVE_FOR_HALF = 0x100
        FAIL_FOR_HALF = 0x200
        NO_WAKE_SLEEPERS = 0x400

        @property
        def is_drain(self) -> bool:
            return bool(self & self.DAMAGE_DRAIN | self.DRAIN_DAMAGE_NO_STACK)

        @property
        def is_reverse_drain(self) -> bool:
            return bool(
                self & self.REVERSE_DAMAGE_DRAIN | self.REVERSE_DAMAGE_DRAIN_NO_STACK
            )

    # TODO: value MINHITPOINTS
    @cached_property
    def p1(self):
        value = (
            super().p1
            + Dice(
                self.dice, luck=self.source.LUCK - self.target.stats.LUCK.value
            ).average()
        )
        return int(value)

    @cached_property
    def damage_type(self) -> int:
        return Dmgtype(super().p2 & 0xFFFF0000)

    @cached_property
    def damage_mode(self) -> int:
        return super().p2 & 0xFFFF

    @cached_property
    def special(self):
        return self.SpecialFlags(super().special)

    @cached_property
    def is_instant(self) -> bool:
        # Les points de vie peuvent être modifiés pour une durée
        if self.duration > 0 and (
            self.special.is_drain or self.special.is_reverse_drain
        ):
            return False
        return super().is_instant

    @cached_property
    def timing_mode(self) -> TimingModes:
        if not self.is_instant:
            return TimingModes.INSTANT
        return super().timing_mode

    def is_valid(self):
        if not self.target and (self.special.is_drain or self.special.is_reverse_drain):
            return False
        # TODO
        elif (
            self.special == self.SpecialFlags.FIST_ONLY
        ):  # and not self.source.is_fist:
            return False
        return True

    def get_saving_throws_desc(self) -> str:
        current_impact = super().get_saving_throws_desc()
        if (
            self.saving_throw & JSChoices.ALL
            and self.special & self.SpecialFlags.SAVE_FOR_HALF
        ):
            current_impact = JSImpactDisplay.HALF.value
        return current_impact


class ClassPlabMixin:
    def is_valid(self) -> bool:
        return super().is_valid() or self.p2 == 3

    @cached_property
    def p1(self):
        if self.p2 == 3:
            dice_size = 1
            try:
                datas = da.CLASSPLAB
            except AttributeError:
                pass
            else:
                klass_name = ids.CLASS.get(self.target.klass)
                if klass_name in datas:
                    dice_size = int(getattr(datas[klass_name], self.stat_name, 1))

            value = round(Dice((1, dice_size)).average())
            return min(
                value, self.plab_max - self.target.stats[self.stat_name].permanent
            )
        return super().p1

    @cached_property
    def p2(self):
        p2 = super().p2
        if p2 == 3 and not self.first_apply:
            return 0
        return p2


class Opcode15(ClassPlabMixin, OpcodeCarac):
    stat_name = "DEX"
    plab_max = 21


# TODO
class Opcode17(OpcodeBase):
    stat_name = "HITPOINTS"
    have_dice = True

    @cached_property
    def p1(self):
        p1 = super().p1
        p1 = signed_byte_value(p1, 32)
        return p1 + round(Dice(self.dice, luck=self.target.stats.LUCK.value).average())

    def apply_on(self):
        from models.effect import EffectV1

        mod, method = split_32bit(self.p2)
        if self.target.stats.HITPOINTS.value <= 0:
            if not mod & 0x1:
                return
            raise_effect = EffectV1.create_default(
                opcode_number=32, probability_end=100
            )
            raise_effect.apply(self.source, self.target)
        elif mod == 0x2:  # dispel
            self.target.current_effects = [
                eff
                for eff in self.target.current_effects
                if eff.timing_mode == TimingModes.REALLY_PERMANENT
            ]
            self.target.mod_state &= ~State.STATE_POISONED
            # TODO: dissipation de l'arme magique
            self.target.load()


class Opcode18(OpcodeBase):
    stat_name = "MAXHITPOINTS"
    have_dice = True

    def is_valid(self):
        return (
            0 <= self.p2 <= 6
            and not self.target.state & State.STATE_REALLY_DEAD
            and self.target.stats.HITPOINTS.value > 0
        )

    @cached_property
    def p1(self):
        p1 = super().p1
        p1 = signed_byte_value(p1, 16)
        p1 += round(Dice(self.dice, luck=self.target.stats.LUCK.value).average())

        if self.p2 == 6:
            max_value = 0
            for effect in self.target.current_effects:
                opcode = effect.opcode
                if opcode.opcode_number == self.opcode_number and opcode.p2 == self.p2:
                    max_value = max(max_value, opcode.p1)
            p1 -= max_value
        return p1

    @cached_property
    def p2(self):
        p2 = super().p2
        if self.timing.is_permanent and p2 == 6:
            p2 = 0
        return p2

    def apply_on(self) -> None:
        self.add_hitpoint = 0
        stat_name = self.stat_name

        if self.timing.is_permanent or self.p2 in (1, 4):
            current_value = self.target.stats[stat_name].permanent
        else:
            current_value = self.target.stats[stat_name].mod

        match self.p2:
            case 0 | 3 | 6:
                next_value = current_value + self.p1
            case 1 | 4:
                next_value = self.p1
            # TODO: confirmer mode 2
            case 2 | 5:
                next_value = (
                    int(self.p1 / 100 - 1) * self.target.stats[stat_name].permanent
                )
                next_value = signed_byte_value(next_value, 16)
            case _:
                return

        if self.timing.is_permanent or self.p2 in (1, 4):
            self.target.stats[stat_name].permanent = next_value
        else:
            self.target.stats[stat_name].mod = next_value

        if self.first_apply:
            change_currenthp = self.p2 in (0, 1, 2, 6) and not (self.special & 0x1)
            if change_currenthp:
                pass
                # FIXME
                # self.target.stats["HITPOINTS"].permanent += next_value


class Opcode19(OpcodeCarac):
    stat_name = "INT"


class Opcode27(OpcodeResist):
    stat_name = "RESISTACID"


class Opcode28(OpcodeResist):
    stat_name = "RESISTCOLD"


class Opcode29(OpcodeResist):
    stat_name = "RESISTELECTRICITY"


class Opcode30(OpcodeResist):
    stat_name = "RESISTFIRE"


class Opcode31(OpcodeResist):
    stat_name = "MAGICDAMAGERESISTANCE"
    max_value = 100


class OpcodeJS(OpcodeStats):
    min_value = -20
    max_value = 20
    mult = -1

    # TODO: gestion p2 == 3 + bug du 33
    def is_valid(self):
        return self.p2 <= 3

    @cached_property
    def p2(self):
        return super().p2 % 3


class Opcode33(OpcodeJS):
    stat_name = "SAVEVSDEATH"


class Opcode34(OpcodeJS):
    stat_name = "SAVEVSWANDS"


class Opcode35(OpcodeJS):
    stat_name = "SAVEVSPOLY"


class Opcode36(OpcodeJS):
    stat_name = "SAVEVSBREATH"


class Opcode37(OpcodeJS):
    stat_name = "SAVEVSSPELL"


class Opcode44(ClassPlabMixin, OpcodeCarac):
    stat_name = "STR"
    plab_max = 20


class Opcode49(OpcodeCarac):
    stat_name = "WIS"


class Opcode67(OpcodeBase):
    def is_valid(self) -> bool:
        return bool(self.resource1)

    def get_zone_restriction(self) -> str:
        return TargetType.SPECIAL


class Opcode74(OpcodeBase):
    @cached_property
    def duration(self):
        duration = super().duration
        if self.p2 != 0:
            timing_mode = super().timing_mode
            if timing_mode in (
                TimingModes.DELAY_INSTANT,
                TimingModes.DELAY_INSTANT_ABS,
            ):
                dice_size, dice_count = split_32bit(self.p1)
                duration = round(
                    Dice(
                        (dice_count, dice_size), luck=self.target.stats.LUCK.value
                    ).average()
                    * 100
                )
            else:  # durée indéfinissable
                duration = -1
        return duration

    @cached_property
    def timing_mode(self):
        timing_mode = super().timing_mode
        if self.p2 != 0 and timing_mode not in (
            TimingModes.DELAY_INSTANT,
            TimingModes.DELAY_INSTANT_ABS,
        ):
            return TimingModes.INSTANT_ABS
        return timing_mode

    def apply_on(self):
        if not self.timing.is_permanent:
            self.target.mod_state |= State.STATE_BLIND
        elif self.first_apply:
            self.target.permanent_state |= State.STATE_BLIND

        # effets non cumulatifs
        for effect in self.target.current_effects:
            if effect.opcode.opcode_number == self.opcode_number:
                return
        self.target.stats["ARMORCLASS"].mod += 4
        self.target.stats["THAC0"].mod += 4


class Opcode84(OpcodeResist):
    stat_name = "RESISTMAGICFIRE"


class Opcode85(OpcodeResist):
    stat_name = "RESISTMAGICCOLD"


class Opcode86(OpcodeResist):
    stat_name = "RESISTSLASHING"
    max_value = 100


class Opcode87(OpcodeResist):
    stat_name = "RESISTCRUSHING"
    max_value = 100


class Opcode88(OpcodeResist):
    stat_name = "RESISTPIERCING"
    max_value = 100


class Opcode89(OpcodeResist):
    stat_name = "RESISTMISSILE"
    max_value = 100


class Opcode97(OpcodeCarac):
    stat_name = "STREXTRA"
    min_value = 1
    max_value = 100


class Opcode101(OpcodeBase):
    def apply_on(self):
        self.target.immune_opcodes.add(self.p2)


class Opcode104(OpcodeStats):
    stat_name = "XP"


class Opcode105(OpcodeStats):
    stat_name = "GOLD"
    min_value = 0
    # TODO: cas particuliers pour les membres du groupe

    @cached_property
    def p1(self):
        value = super().p1
        if self.p2 == 0 and self.target.is_in_party:
            value *= -1
        return value

    @cached_property
    def is_savable(self):
        return not self.is_permanent

    @cached_property
    def is_permanent(self):
        return super().is_permanent or self.target.is_in_party

    def apply_on(self):
        if not self.target.is_in_party:
            return super().apply_on()


class Opcode106(OpcodeStats):
    stat_name = "MORALEBREAK"
    min_value = 0
    max_value = 20

    def is_valid(self):
        return super().is_valid() and not (self.target.state & State.STATE_BERSERK)


class Opcode108(OpcodeStats):
    stat_name = "REPUTATION"
    min_value = 10
    max_value = 200

    # TODO: méthode 3 à 5
    def is_valid(self):
        return 0 <= self.p2 <= 5

    @cached_property
    def is_savable(self):
        return not self.is_permanent

    @cached_property
    def is_permanent(self):
        return super().is_permanent or self.p2 >= 3

    def apply_on(self):
        if self.p2 <= 2:
            return super().apply_on()


class Opcode109(OpcodeBase):
    def is_valid(self):
        return (
            self.p2 > 9
            or self.p2 < 2
            or self.false_petrify
            or check_match_target(self, self.target)
        ) and not (self.target.state & State.STATE_DEAD)

    def get_zone_restriction(self):
        if self.special != 0:
            return TargetType.NON_PARTY
        if self.p1 != 0 and (restriction := translate_creature_type(self.p2, self.p1)):
            return restriction
        return super().get_zone_restriction()

    @property
    def false_petrify(self) -> bool:
        return self.special != 0 and not self.target.is_in_party

    def apply_on(self):
        self.target.mod_state |= State.STATE_HELPLESS

        if self.false_petrify:
            self.target.mod_state |= State.STATE_STONE_DEATH
        else:
            self.target.stats["HELD"].set = 1


class Opcode125(OpcodeBase):
    def get_zone_restriction(self) -> str:
        return TargetType.LOCK


class Opcode127(OpcodeBase):
    have_dice = True

    def get_zone_restriction(self) -> str:
        return TargetType.SPECIAL


class Opcode129(OpcodeBase):
    def is_valid(self):
        return self.target.stats.HITPOINTS.value > 0

    @cached_property
    def p2(self):
        if self.first_apply:
            value = round(Dice((self.p1 & 0x7FFF, 8)).average())
        else:
            value = signed_byte_value(super().p2, 16)
        return value & 0xFFFF

    def apply_on(self):
        if self.target.state & State.STATE_AID:
            return

        self.target.mod_state |= State.STATE_AID
        mod_value = signed_byte_value(self.p1, 8)
        for stat_name in st_stat_name:
            self.target.stats[stat_name].mod -= mod_value
        self.target.stats["THAC0"].mod -= mod_value
        self.target.stats["DAMAGEBONUS"].mod += mod_value
        self.target.stats["MAXHITPOINTS"].mod += self.p2

        if self.first_apply:
            self.target.heal(self.p2)


class Opcode130(OpcodeBase):
    @cached_property
    def p1(self):
        return signed_byte_value(super().p1, 8)

    def apply_on(self):
        if self.target.state & State.STATE_BLESS:
            return

        self.target.mod_state |= State.STATE_BLESS
        self.target.stats["DAMAGEBONUS"].mod += self.p1
        self.target.stats["THAC0"].mod -= self.p1
        if self.first_apply:
            self.target.stats.MORALE.value += self.p1 * 4


class Opcode131(OpcodeBase):
    @cached_property
    def p1(self):
        return signed_byte_value(super().p1, 8)

    def apply_on(self):
        if self.target.state & State.STATE_CHANT:
            return

        self.target.mod_state |= State.STATE_CHANT
        self.target.stats["LUCK"].mod += self.p1


class Opcode132(OpcodeBase):
    @cached_property
    def p1(self):
        return signed_byte_value(super().p1, 8)

    def apply_on(self):
        if self.target.state & State.STATE_DRAWUPONHOLYMIGHT:
            return

        self.target.mod_state |= State.STATE_DRAWUPONHOLYMIGHT
        self.target.stats["STR"].mod += self.p1
        self.target.stats["DEX"].mod += self.p1
        self.target.stats["CON"].mod += self.p1


class Opcode133(OpcodeBase):
    @cached_property
    def p1(self):
        return signed_byte_value(super().p1, 8)

    def apply_on(self):
        if self.target.state & State.STATE_LUCK:
            return

        self.target.mod_state |= State.STATE_LUCK
        self.target.stats["LUCK"].mod += self.p1OTI


class Opcode134(OpcodeBase):
    def apply_on(self):
        self.target.mod_state |= State.STATE_STONE_DEATH
        self.target.stats.HITPOINTS.mod = 0


class Opocode136(OpcodeBase):
    def apply_on(self):
        self.target.dispel_effects(opcode_number=20, p2=0)
        self.target.mod_state &= ~State.STATE_INVISIBLE


class Opcode137(OpcodeBase):
    @cached_property
    def p1(self):
        return signed_byte_value(super().p1, 8)

    def apply_on(self):
        if self.target.state & State.STATE_CHANTBAD:
            return

        self.target.mod_state |= State.STATE_CHANTBAD
        self.target.stats["LUCK"].mod -= self.p1


class Opcode146(OpcodeBase):
    def is_valid(self):
        return bool(self.resource1 and self.timing.timing_mode != TimingModes.EQUIPED)

    def apply_on(self):
        super().apply_on()
        for child in self.children:
            child.apply(source=self.target, target=self.target)

    @cached_property
    def spell_level(self) -> int | None:
        match self.p2:
            case 0:
                return self.p1 if self.p1 > 0 else None
            case 2:
                return self.p1
            case _:
                return None

    @cached_property
    def child_is_instant(self) -> bool:
        return self.p1 != 0

    @cached_property
    def children(self):
        from models import Spell

        child = Spell.create_from_filename(
            self.resource1, casting_is_instant=self.child_is_instant
        )
        if self.spell_level is None:
            if hasattr(self, "source"):
                child = child.from_source(self.source)
        else:
            child = child.from_level(self.spell_level)

        children = super().children
        return children + (child,)


class Opcode148(Opcode146):
    @cached_property
    def spell_level(self) -> int | None:
        if self.p1 == 0 or self.p2 != 0:
            return None
        return self.p1


class Opcode153(OpcodeBase):
    def apply_on(self):
        self.target.stats["SANCTUARY"].set = 1


class Opcode154(OpcodeBase):
    def apply_on(self):
        self.target.stats["ENTANGLE"].set = 1


class Opcode155(OpcodeBase):
    def apply_on(self):
        self.target.stats["MINORGLOBE"].set = 1


class Opcode156(OpcodeBase):
    def is_valid(self) -> bool:
        return super().is_valid() and not self.target.state & State.STATE_DEAD

    def apply_on(self):
        self.target.stats["SHIELDGLOBE"].set = 1


class Opcode157(OpcodeBase):
    def apply_on(self):
        self.target.stats["WEB"].set = 1
        self.target.stats["HELD"].set = 1
        self.target.mod_state |= State.STATE_HELPLESS


class Opcode158(OpcodeBase):
    def apply_on(self):
        self.target.stats["GREASE"].set = 1


class Opcode159(OpcodeBase):
    def is_valid(self) -> bool:
        return super().is_valid() and not self.target.state & State.STATE_DEAD

    def apply_on(self):
        self.target.mod_state |= State.STATE_MIRRORIMAGE
        # TODO: où est stocké p1 ????


class Opcode160(OpcodeBase):
    def apply_on(self):
        self.target.dispel_effects(opcode_number=153)
        self.target.stats["SANCTUARY"].set = 0


class Opcode161(OpcodeBase):
    def apply_on(self):
        self.target.dispel_effects(opcode_number=24)
        self.target.dispel_effects(opcode_number=142, p2=36)
        self.target.mod_state &= ~State.STATE_PANIC


class Opcode162(OpcodeBase):
    def apply_on(self):
        self.target.dispel_effects(opcode_number=109)
        self.target.dispel_effects(opcode_number=175)
        self.target.dispel_effects(opcode_number=142, p2=13)
        self.target.stats["HELD"].set = 0


class Opcode163(OpcodeBase):
    def apply_on(self):
        self.target.dispel_effects(opcode_number=126, p2=1)


class Opcode166(OpcodeResist):
    stat_name = "RESISTMAGIC"

    def is_valid(self):
        return self.p2 in {0, 1}


class Opcode173(OpcodeResist):
    stat_name = "RESISTPOISON"
    max_value = 100

    @cached_property
    def p1(self):
        p1 = min(0xFF, super().p1)
        return signed_byte_value(p1, 8)

    def apply_on(self):
        self.target.stats[self.stat_name] = min(
            self.target.stats[self.stat_name] + self.p1, self.max_value
        )


class Opcode175(Opcode109):
    def get_zone_restriction(self):
        if self.p1 != 0 and (restriction := translate_creature_type(self.p2, self.p1)):
            return restriction
        return super().get_zone_restriction()

    @property
    def false_petrify(self):
        return False


class Opcode177(OpcodeBase):
    @cached_property
    def is_savable(self):
        return False

    def get_zone_restriction(self):
        if self.p1 != 0 and (restriction := translate_creature_type(self.p2, self.p1)):
            return restriction
        return super().get_zone_restriction()

    @cached_property
    def is_used(self):
        return any(child.opcode.is_used for child in self.children)

    def is_valid(self):
        return (
            2 <= self.p2 <= 9
            and bool(self.resource1)
            and check_match_target(self, self.target)
        )

    def apply_on(self) -> None:
        for child in self.children:
            child.apply(self.source, self.target)

    @cached_property
    def children(self):
        from models.effect import EffectV2

        child = EffectV2.create_from_filename(
            self.resource1,
            timing_mode=self.timing_mode,
            duration=self.duration,
        )
        children = super().children
        return children + (child,)


class Opcode185(Opcode175):
    pass


class Opcode192(OpcodeBase):
    def get_zone_restriction(self) -> str:
        return TargetType.FAMILAR


class Opcode208(OpcodeBase):
    def apply_on(self):
        self.target.stats["MINHITPOINTS"].set = self.p1


class Opcode209(OpcodeBase):
    def is_valid(self):
        return super().is_valid() and self.target.stats.HITPOINTS.value < 60

    def apply_on(self):
        from models.effect import EffectV2

        EffectV2(
            opcode_number=13,
            p2=4,
            probability_end=100,
        ).apply(self.source, self.target)


class Opcode210(OpcodeBase):
    def is_valid(self):
        return super().is_valid() and self.target.stats.HITPOINTS.value < 90

    @cached_property
    def timing_mode(self):
        return TimingModes.INSTANT_ABS

    @cached_property
    def duration(self):
        if not hasattr(self, "target"):
            return -1

        dice_count = 1
        if self.target.stats.HITPOINTS.value < 30:
            dice_count = 4
        elif self.target.stats.HITPOINTS.value < 60:
            dice_count = 2

        return (
            round(Dice((dice_count, 4), luck=self.target.stats.LUCK.value).average())
            * TimeUnits.ROUND.tick_duration
        )

    def apply_on(self):
        from models.effect import EffectV1

        EffectV1(
            opcode_number=45,
            timing=self.timing_mode,
            duration=self.duration,
            probability_end=100,
        ).apply(self.source, self.target)


class Opcode213(OpcodeBase):
    def apply_on(self):
        # TODO

        from models.effect import EffectV2

        EffectV2(
            opcode_number=212,
            p2=self.p2,
            timing=TimingModes.DELAY_PERMANENT,
            duration=self.duration,
            probability_end=100,
        ).apply(self.source, self.target)

    @cached_property
    def timing_mode(self):
        if self.p2 == 0:
            return TimingModes.INSTANT
        return super().timing_mode

    @cached_property
    def duration(self):
        if self.p2 != 0:
            return super().duration
        if not hasattr(self, "target"):
            return -1

        int_score = self.target.stats["INT"].permanent
        values = da.INTMOD[int_score]
        return (
            round(
                Dice(
                    (
                        int(values.MAZE_DURATION_DICE_NUM),
                        int(values.MAZE_DURATION_DICE_SIZE),
                    ),
                    luck=self.target.stats.LUCK.value,
                ).average()
            )
            * 6
        )


class Opcode214(OpcodeBase):
    @cached_property
    def duration(self):
        return -1

    def get_saving_throws_desc(self) -> str:
        return JSImpactDisplay.SPECIAL.value

    def get_zone_restriction(self) -> str:
        return TargetType.SPECIAL


class Opcode216(OpcodeBase):
    def apply_on(self):
        self.target.stats["LEVELDRAIN"].mod += self.p1

        if self.target.level_average() <= 0:
            self.target.flags |= 0x100000

            from models.effect import EffectV2

            EffectV2(
                opcode_number=13,
                p2=0x40,
                probability_end=100,
            ).apply(self.source, self.target)


class Opcode217(OpcodeBase):
    def is_valid(self):
        return super().is_valid() and self.target.stats.HITPOINTS.value < 20

    @cached_property
    def duration(self):
        return 30

    @cached_property
    def timing_mode(self):
        return TimingModes.INSTANT

    def apply_on(self):
        from models.effect import EffectV1

        EffectV1(
            opcode_number=39,
            p2=self.p2,
            special=self.special,
            timing=self.timing,
            duration=self.duration,
            probability_end=100,
        ).apply(self.source, self.target)


class Opcode218(OpcodeBase):
    def is_valid(self):
        return super().is_valid() and not (self.target.state | State.STATE_DEAD)

    @cached_property
    def have_dice(self):
        return self.p2 != 0

    @cached_property
    def p1(self):
        value = super().p1
        if self.have_dice:
            return int(value + Dice(self.dice).average())
        return value

    def apply_on(self):
        self.target.stats_mod["STONESKINS"] = self.p1


# Arrêt du temps
class Opcode231(OpcodeBase):
    @cached_property
    def duration(self):
        return round(super().duration / 2)


class Opcode241(Opcode5):
    pass


class Opcode255(OpcodeBase):
    @cached_property
    def duration(self):
        return super().duration * TimeUnits.DAY.tick_duration


class Opcode283(Opcode177):
    pass


class Opcode314(Opcode218):
    @cached_property
    def have_dice(self):
        return False

    def apply_on(self):
        self.target.stats_mod["STONESKINSGOLEM"] = self.p1


class Opcode325(OpcodeJS):
    min_value = 0

    def apply_on(self):
        for stat_name in st_stat_name:
            self.stat_name = stat_name
            super().apply_on()
        self.stat_name = ""


class Opcode326(Opcode146):
    child_is_instant = True

    # TODO
    def is_valid(self):
        return True

    @cached_property
    def spell_level(self) -> int | None:
        return None


class Opcode331(Opcode127):
    pass


class Opcode333(OpcodeBase):
    have_dice = True


class Opcode346(OpcodeStats):
    def is_valid(self):
        return 0 <= self.p2 <= 1 and self.special in SchoolChoices._value2member_map_

    def apply_on(self):
        stat_name = SchoolChoices(self.p2).stat_name

        if self.p2 == 0:
            self.target.stats[stat_name].mod += self.p1
        else:
            self.target.stats[stat_name].set = self.p1


opcode_class_names = set(opcode_classname(opcode_number) for opcode_number in OPCODES)
# Création dynamique des classes restantes
for class_name in opcode_class_names - globals().keys():
    globals()[class_name] = type(class_name, (OpcodeBase,), {})

# Génère automatiquement le dictionnaire des opcodes : {"OpcodeX": opcodeClass}
opcode_mapping = dict(inspect.getmembers(sys.modules[__name__], inspect.isclass))
