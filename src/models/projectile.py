from functools import cached_property

from settings import GAME_VERSION
from utils import BaseType, Byte, Char, Dword, Resref, Word  # StrrefPK
from utils.common import distance_str
from utils.enums import Duration, TargetType, ZoneDesc
from utils.timing import Timing, TimingModes

from .common import BgBase


class Projectile(BgBase):
    ext = ".pro"
    expected_signature = "PRO "
    id = 0
    fields = {
        "signature": Char(),
        "version": Char(),
        "type": Word(),
        "speed": Word(),
        "flags": Dword(),
        "truc": BaseType(size=28),
        # "fire_sound": Resref(klass=None),
        # "impact_sound": Resref(klass=None),
        # "source_animation": Resref(klass=None),
        # "color": Word(klass=None),
        # "width": Word(klass=None),
        "flags2": Dword(),
        "truc2": BaseType(size=20),
        # "message": StrrefPK(klass=None),
        # "pulse_color": Dword(klass=None),
        # "color_speed": Word(klass=None),
        # "shake_amount": Word(klass=None),
        # "creature_value_1": Word,
        # "creature_type_1": Word,
        # "creature_value_2": Word,
        # "creature_type_2": Word,
        "default_spell": Resref(),
        "success_spell": Resref(),
        "unused": BaseType(size=172),
        # BAM (if single target or area effect)
        "BAM": BaseType(size=256),
        # if area effect
        "area_flags": Word(),
        "ray_count": Word(),
        "trigger_radius": Word(),
        "area": Word(signed=True),
        "explosion_sound": Resref(klass=None),
        "explosion_delay": Word(),
        "fragment_animation": Word(klass=None),
        "secondary_projectile": Word(),
        "trigger_count": Byte(),
        "explosion_effect": Byte(),
        "explosion_color": Byte(klass=None),
        "unused2": Byte(klass=None),
        "explosion_projectile": Word(),
        "explosion_animation": Resref(klass=None),
        "cone_shape": Word(),
        "rotate_rays": Word(klass=None),
        "spread_animation": Resref(klass=None),
        "area_sound": Resref(klass=None),
        "area_flags2": Dword(),
        "dice_thrown": Word(),
        "dice_sides": Word(),
        # "animation_granularity": Word(klass=None),
        # "animation_granularity_divider": Word(klass=None),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Projectile de `Appel de la foudre`
        if GAME_VERSION.startswith("BG") and self.id in (81, 82, 83, 84):
            self.trigger_count = self.id - 80
            self.explosion_delay = 90
        # Projectile `SCORCHER`
        elif self.is_scorcher:
            self.type = 3
            self.area = 30
            self.trigger_count = 1
            self.explosion_delay = 90

    @cached_property
    def is_one_target(self) -> bool:
        return self.type == 2

    @cached_property
    def is_area(self) -> bool:
        return self.type == 3

    @cached_property
    def radius_effect(self) -> float:
        if self.is_area:
            # Uses absolute value, or negative values use the same area as their positive counterpart.
            return abs(self.area) / 2.0
        return 0.0

    @cached_property
    def is_scorcher(self) -> bool:
        return self.filename.startswith("SCORCHER")

    def get_cone_shape(self) -> int:
        if self.is_area and self.area_flags & 0x800:
            return self.cone_shape
        return 0

    def get_zone_restriction(self) -> str:
        restriction = TargetType.NONE
        if self.target_only_ennemies:
            restriction = TargetType.ENEMY
        elif self.target_only_party:
            restriction = TargetType.ALLY
        return restriction

    def get_radius_desc(self):
        return distance_str(self.radius_effect / 8.0)

    def get_zone_desc(self) -> str:
        if self.get_cone_shape():
            area_strref = ZoneDesc.CONE
        elif self.is_scorcher:
            area_strref = ZoneDesc.SCORCHER
        elif self.is_area:
            area_strref = ZoneDesc.AREA
        else:
            area_strref = ZoneDesc.NONE

        return area_strref

    @property
    def target_only_party(self) -> bool:
        return self.is_area and self.area_flags & 0xC0 == 0xC0

    @property
    def target_only_ennemies(self) -> bool:
        return self.is_area and self.area_flags & 0xC0 == 0x40

    @property
    def is_trigger(self) -> bool:
        return self.is_area and self.area_flags & 0x4

    def get_duration(self) -> int:
        return self.trigger_count * self.explosion_delay

    def get_duration_desc(self) -> str | None:
        if self.is_trigger:
            duration_desc = Duration.TRIGGER.value
        # FIXME: trouver une meilleure solution
        elif not (duration := self.get_duration()) or (
            self.trigger_count <= 1 and self.explosion_delay == 100
        ):
            return None
        else:
            duration_desc = Timing(TimingModes.INSTANT_ABS, duration).get_desc()

        frequency = Timing(TimingModes.INSTANT_ABS, self.explosion_delay).get_desc()
        effect_count = self.trigger_count
        strref = (
            _("{duration}")
            if effect_count <= 1
            else _("{duration} ({effect_count} cycles de {frequency})")
        )
        return strref.format(
            duration=duration_desc, effect_count=effect_count, frequency=frequency
        )
