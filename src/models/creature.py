from collections import defaultdict
from contextlib import contextmanager
from functools import cached_property

from files import da, ids
from models.effect import EffectV2Min
from models.item import Item
from models.stat import stat_mapping
from settings import NIGHTMARE_MODE_ACTIVE
from utils import BaseType, Byte, Char, Charaway, Dword, Resref, StrrefPK, Word
from utils.effect_list import EffectList
from utils.enums import ISlot, KitChoices, SchoolChoices
from utils.enums.ids_reverse import Ea, Itemflag, State

from .common import BgBase


class ItemSlotResRef(Resref):
    def read_offset(self, offset):
        value = super().read_offset(offset)

        return self.model.create_from_filename(value)


class ItemSlot(BgBase):
    fields = {
        "item": ItemSlotResRef(model=Item),
        "expiration_time": Byte(),
        "expiration_time2": Byte(),
        "charge1": Word(),
        "charge2": Word(),
        "charge3": Word(),
        "flags": Dword(),
    }


class MemorisedSpell(BgBase):
    fields = {
        "filename": Resref(),
        "memorised": Dword(),
    }


class MemorisedSpellInfo(BgBase):
    fields = {
        "level": Word(),
        "number_memorizable": Word(),
        "number_memorised": Word(),
        "spell_type": Word(),
        "index": Dword(),
        "memorised_count": Dword(),
    }


class KnownSpell(BgBase):
    fields = {
        "filename": Resref(),
        "level": Word(),
        "spell_type": Word(),
    }


class SlotPosition(BgBase):
    fields = {"index": Word(signed=True)}


class Creature(BgBase):
    ext = ".cre"
    expected_signature = "CRE "
    fields = {
        "signature": Char(),
        "version": Char(),
        "long_name": StrrefPK(),
        "short_name": StrrefPK(),
        "flags": Dword(),
        "xp": Dword(),  # after kill
        "power_level": Dword(),  # pour sort d'invocation ou expérience de la créature après avoir rejoint le groupe
        "gold": Dword(),
        "init_state": Dword(),
        "current_hp": Word(),
        "max_hp": Word(),
        "animation": Dword(),
        "colors": Byte(size=7, klass=None),
        # "metal_colour_index": Byte,
        # "minor_colour_index": Byte,
        # "major_colour_index": Byte,
        # "skin_colour_index": Byte,
        # "leather_colour_index": Byte,
        # "armor_colour_index": Byte,
        # "hair_colour_index": Byte,
        "eff_version": Byte(),
        "small_portrait": Resref(),
        "large_portrait": Resref(),
        "reputation": Byte(signed=True),
        "hide_in_shadows": Byte(),
        "ca_natural": Word(signed=True),
        "ca_effective": Word(signed=True),
        "ca_crushing": Word(signed=True),
        "ca_missile": Word(signed=True),
        "ca_piercing": Word(signed=True),
        "ca_slashing": Word(signed=True),
        "thaco": Byte(),
        "number_of_attacks": Byte(),
        "save_death": Byte(),
        "save_wands": Byte(),
        "save_polymorph": Byte(),
        "save_breath": Byte(),
        "save_spells": Byte(),
        "resist_fire": Byte(),
        "resist_cold": Byte(),
        "resist_electricity": Byte(),
        "resist_acid": Byte(),
        "resist_magic": Byte(),
        "resist_magic_fire": Byte(),
        "resist_magic_cold": Byte(),
        "resist_slashing": Byte(),
        "resist_crushing": Byte(),
        "resist_piercing": Byte(),
        "resist_missile": Byte(),
        "detect_illusion": Byte(),
        "set_traps": Byte(),
        "lore": Byte(),
        "lock_picking": Byte(),
        "move_silently": Byte(),
        "find_traps": Byte(),
        "pick_pockets": Byte(),
        "fatigue": Byte(),
        "intoxication": Byte(),
        "luck": Byte(),
        "large_sword_proficiency": Byte(),  # BGSWORD
        "small_sword_proficiency": Byte(),  # DAGGER, SMSWORD
        "bow_proficiency": Byte(),  # BOW
        "spear_proficiency": Byte(),  # SPEAR, POLEARM
        "blunt_proficiency": Byte(),  # MACE, HAMMER, STAFF
        "spiked_proficiency": Byte(),  # MSTAR, FLAIL
        "axe_proficiency": Byte(),  # AXE
        "missile_proficiency": Byte(),  # SLING, DART, XBOW
        "proficiencies": Dword(size=7),
        "nightmare_mode": Byte(),
        "translucency": Byte(),
        "reputation_kill": Byte(),
        "reputation_join": Byte(),
        "reputation_leave": Byte(),
        "turn_undead_level": Byte(),
        "tracking_skill": Byte(),
        "tracking_target": Charaway(),
        "strrefs": Resref(size=Resref.size * 50, klass=None),
        "level1": Byte(),
        "level2": Byte(),
        "level3": Byte(),
        "sex": Byte(),
        "strength": Byte(),
        "strength_bonus": Byte(),
        "intelligence": Byte(),
        "wisdom": Byte(),
        "dexterity": Byte(),
        "constitution": Byte(),
        "charisma": Byte(),
        "morale": Byte(),
        "morale_break": Byte(),
        "racial_enemy": Byte(),
        "morale_recovery_time": Word(),
        "kit": Dword(),
        "script_override": Resref(),
        "script_class": Resref(),
        "script_race": Resref(),
        "script_general": Resref(),
        "script_default": Resref(),
        "ea": Byte(),
        "general": Byte(),
        "race": Byte(),
        "klass": Byte(),
        "specific": Byte(),
        "gender": Byte(),
        "objects": Byte(size=5, klass=None),
        # "object_1": Byte,
        # "object_2": Byte,
        # "object_3": Byte,
        # "object_4": Byte,
        # "object_5": Byte,
        "alignment": Byte(),
        "global_value": Word(),
        "local_value": Word(),
        "script_name": Charaway(),
        "known_spell_offset": Dword(),  # présent dans le livre de sort
        "known_spell_count": Dword(),
        "memorised_info_spell_offset": Dword(),  # combien de slot pour les sorts
        "memorised_info_spell_count": Dword(),
        "memorised_spell_offset": Dword(),  # combien de sorts sont actuellement disponibles
        "memorised_spell_count": Dword(),
        "item_slot_offset": Dword(),
        "item_offset": Dword(),
        "item_count": Dword(),
        "effect_offset": Dword(),
        "effect_count": Dword(),
        "dialog_file": Resref(),
        "known_spells": BaseType(
            klass=list, model=KnownSpell, nb_key="known_spell_count"
        ),
        "memorised_info_spells": BaseType(
            klass=list, model=MemorisedSpellInfo, nb_key="memorised_info_spell_count"
        ),
        "memorised_spells": BaseType(
            klass=list, model=MemorisedSpell, nb_key="memorised_spell_count"
        ),
        "init_effects": BaseType(klass=list, model=EffectV2Min, nb_key="effect_count"),
        "slots": BaseType(klass=list, model=ItemSlot, nb_key="item_count"),
        "map_slots": BaseType(klass=list, model=SlotPosition, nb=40),
    }

    immune_permanent_state = 0x0
    immune_current_state = 0x0

    is_in_party: bool = False

    @property
    def _base_stats(self):  # 0434
        return {
            "HITPOINTS": self.data_initial["current_hp"],
            "MAXHITPOINTS": self.data_initial["max_hp"],
            "ARMORCLASS": min(
                self.data_initial["ca_natural"], self.data_initial["ca_effective"]
            ),
            "ACCRUSHINGMOD": self.data_initial["ca_crushing"],
            "ACMISSILEMOD": self.data_initial["ca_missile"],
            "ACPIERCINGMOD": self.data_initial["ca_piercing"],
            "ACSLASHINGMOD": self.data_initial["ca_slashing"],
            "THAC0": self.data_initial["thaco"],
            "NUMBEROFATTACKS": self.data_initial["number_of_attacks"],
            "SAVEVSDEATH": self.data_initial["save_death"],
            "SAVEVSWANDS": self.data_initial["save_wands"],
            "SAVEVSPOLY": self.data_initial["save_polymorph"],
            "SAVEVSBREATH": self.data_initial["save_breath"],
            "SAVEVSSPELL": self.data_initial["save_spells"],
            "RESISTFIRE": self.data_initial["resist_fire"],
            "RESISTCOLD": self.data_initial["resist_cold"],
            "RESISTELECTRICITY": self.data_initial["resist_electricity"],
            "RESISTACID": self.data_initial["resist_acid"],
            "RESISTMAGIC": self.data_initial["resist_magic"],
            "RESISTMAGICFIRE": self.data_initial["resist_magic_fire"],
            "RESISTMAGICCOLD": self.data_initial["resist_magic_cold"],
            "RESISTSLASHING": self.data_initial["resist_slashing"],
            "RESISTCRUSHING": self.data_initial["resist_crushing"],
            "RESISTPIERCING": self.data_initial["resist_piercing"],
            "RESISTMISSILE": self.data_initial["resist_missile"],
            "LORE": self.data_initial["lore"],
            "LOCKPICKING": self.data_initial["lock_picking"],
            "STEALTH": self.data_initial["move_silently"],
            "TRAPS": self.data_initial["find_traps"],
            "PICKPOCKET": self.data_initial["pick_pockets"],
            "FATIGUE": self.data_initial["fatigue"],
            "INTOXICATION": self.data_initial["intoxication"],
            "LUCK": self.data_initial["luck"],
            "STR": self.data_initial["strength"],
            "STREXTRA": self.data_initial["strength_bonus"],
            "INT": self.data_initial["intelligence"],
            "WIS": self.data_initial["wisdom"],
            "DEX": self.data_initial["dexterity"],
            "CON": self.data_initial["constitution"],
            "CHR": self.data_initial["charisma"],
            "MORALEBREAK": self.data_initial["morale_break"],
            "MORALERECOVERYTIME": self.data_initial["morale_recovery_time"],
            "MORALE": self.data_initial["morale"],
            "LEVEL": self.data_initial["level1"],
            "LEVEL2": self.data_initial["level2"],
            "LEVEL3": self.data_initial["level3"],
            "TURNUNDEADLEVEL": self.data_initial["turn_undead_level"],
            "RESISTPOISON": 0,
            "MAGICDAMAGERESISTANCE": 0,
            "VISUALRANGE": 14,
        } | {school.stat_name: 0 for school in SchoolChoices}

    """
    De base, pour l'équipement, on a deux données :
    1. les slots (`slots`)
        le slot ne renvoie pas directement l'objet mais une instance de `ItemSlot`
        elle contient des informations générales sur le slot contenant l'objet :
        item : l'instance de l'objet contenu, de classe `Item`
        mais aussi les flags, charges, expiration
        le problème c'est la position des éléments de la table ne correspond PAS à l'index réel de l'inventaire
        la correspondance est rendue possible grâce à `map_slots`
    2. la table de correspondance des slots (`map_slots`)
        permet de faire la correspondance entre l'index d'un objet de l'inventaire à une position dans `slots`
        par exemple, si je cherche l'objet dans la main gauche d'un personnage `ISlot.SHIELD` :
        je recherche map_slots[ISlot.SHIELD] pour récupérer une valeur d'index
        puis je peux accéder à l'objet grâce à `slots[index]`
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # liste def effets activés
        self.current_effects = EffectList()
        self.equiped_effects = EffectList()

        self.load()

        # FIXME: les checks (JS notamment) sont réalisés avant la réalisation des effets du sort
        for slot in self.equiped_slots:
            for eff in slot.item.equip_effects:
                eff.apply(self)

        for eff in self.init_effects:
            eff.apply(self)

    @contextmanager
    def alter_effects(self):
        old = self.current_effects.copy()
        try:
            yield
        finally:
            # on ne reload que si des effets présents ont été dissipés
            if old != self.current_effects[: len(old) + 1]:
                self.load()

    def dispel_effects(self, **kwargs) -> None:
        self.equiped_effects = self.equiped_effects.exclude(**kwargs)
        self.current_effects = self.current_effects.exclude(**kwargs)

    @property
    def effects(self) -> EffectList:
        # les effets équipés sont résolus avant les autres
        # on le voit dans la méthode de résolution des opcodes 129 à 134
        return self.equiped_effects + self.current_effects

    @property
    def state(self) -> int:
        return self.init_state | self.mod_state

    @property
    def permanent_state(self) -> int:
        return self.init_state

    @cached_property
    def stuff(self) -> list[Item | None]:
        return [
            slot.item
            for slot in self.item_slots[: ISlot.QUICK_ITEM_1]
            if slot is not None
        ]

    @cached_property
    def item_slots(self) -> list[ItemSlot | None]:
        return [self.get_slot(slot) for slot in ISlot]

    def get_slot_index(self, slot: ISlot | int) -> int:
        # renvoie l'index du slot indiqué, renvoie -1 si vide
        return self.map_slots[slot].index

    def get_slot_from_index(self, index: int) -> ItemSlot | None:
        # renvoie le slot présent à l'index indiqué, renvoie None si le slot est vide
        if max(ISlot) >= index >= 0:
            return self.slots[index]

    def get_slot(self, slot: ISlot | int) -> ItemSlot | None:
        # renvoie le slot d'inventaire correspondant au slot indiqué, None si aucun objet n'est présent
        return self.get_slot_from_index(self.get_slot_index(slot))

    def get_equiped_weapon_slot(self) -> ISlot | None:
        # Priorité aux armes magiques
        if self.get_slot_index(ISlot.MAGIC_WEAPON) != -1:
            return ISlot.MAGIC_WEAPON

        index = self.get_slot_index(ISlot.SELECTED_WEAPON)
        # TODO: à finir
        if 3 >= index >= 0 and self.get_slot_from_index(index):
            return ISlot(index + ISlot.WEAPON_1)

    @property
    def equiped_slots(self) -> list[ItemSlot]:
        inv_slots = ISlot.stuff_slots() - ISlot.weapon_slots() - ISlot.quiver_slots()

        # 9 à 12 : arme
        weapon_slot = self.get_equiped_weapon_slot()
        if weapon_slot is not None:
            inv_slots.add(weapon_slot)
            slot = self.get_slot(weapon_slot)

            # 2 : shield
            # TODO: exclude arme de trait sans munitions dans le carquois
            if slot is not None and slot.item.flags & (
                Itemflag.LEFTHANDED | Itemflag.TWOHANDED
            ):
                inv_slots.discard(ISlot.SHIELD)

            # TODO: 13 à 16 : carquois

        return [slot for inv_slot in inv_slots if (slot := self.get_slot(inv_slot))]

    def load(self) -> None:
        self.stats = Stats(self, initial=self._base_stats)
        self.mod_state = State.STATE_NORMAL

        # immunités
        self.immune_items = set()
        self.immune_opcodes = set()

        self.immune_permanent_state = 0x0
        self.immune_current_state = 0x0

        for eff in self.effects:
            eff.apply_on()

    @property
    def kit(self):
        return KitChoices(self.data_initial["kit"])

    @property
    def level_wizard(self) -> int:
        if self.klass > 21 or self.klass in (1, 5, 13, 19):
            value = self.stats.LEVEL.value
        elif self.klass in (7, 10, 14, 17):
            value = self.stats.LEVEL2.value
        else:
            return 1

        return value + self.stats.CASTINGLEVELBONUSMAGE.value

    # TODO
    @property
    def level_priest(self) -> int:
        value = 1
        if self.klass > 21 or self.klass in (3, 11, 21):
            value = self.stats.LEVEL.value
        elif self.klass == 6:  # PALADIN
            value = self.stats.LEVEL.value - 8
        elif self.klass == 12:  # RANGER
            value = self.stats.LEVEL.value - 7
        elif self.klass == 16:  # FIGHTER/DRUID
            value = self.stats.LEVEL2.value
        else:
            return 1

        return max(1, value + self.stats.CASTINGLEVELBONUSCLERIC.value)

    @property
    def level_highter(self) -> int:
        if self.klass in (7, 8, 9, 13, 14, 15, 16, 18):
            return max(self.stats.LEVEL.value, self.stats.LEVEL2.value)
        if self.klass in (10, 17):
            return max(
                self.stats.LEVEL.value, self.stats.LEVEL2.value, self.stats.LEVEL3.value
            )
        return self.stats.LEVEL.value

    @property
    def level_average(self) -> int:
        if self.klass in (7, 8, 9, 13, 14, 15, 16, 18):
            return round((self.stats.LEVEL.value + self.stats.LEVEL2.value) / 2)
        if self.klass in (10, 17):
            return round(
                (
                    self.stats.LEVEL.value
                    + self.stats.LEVEL2.value
                    + self.stats.LEVEL3.value
                )
                / 3
            )
        return self.stats.LEVEL.value

    @property
    def save_mod(self) -> int:
        # Impact de l'invisibilité majeure et de la difficulté Nightmare sur les JS
        # L'impact n'est pas directement visible sur les JS et sont ajoutés après

        value = 0
        if self.state & State.STATE_IMPROVEDINVISIBILITY:
            value -= 4
        # FIXME: self.current_ea
        if NIGHTMARE_MODE_ACTIVE and self.ea >= Ea.GOODCUTOFF:
            value -= 5
        return value

    def heal(self, value: int) -> None:
        self.stats.HITPOINTS.set = min(
            self.stats.MAXHITPOINTS, self.stats.HITPOINTS + value
        )

    @cached_property
    def exp_max(self) -> int:
        klass_name = ids.CLASS.get(self.klass)
        if klass_name in da.XPCAP:
            value = da.XPCAP[klass_name].VALUE
        else:
            value = da.STARTARE["START_XP_CAP"].VALUE
        return int(value)


class Stats:
    def __init__(self, parent, initial: dict):
        self.parent = parent
        self.mirror_image_nb = 0
        self._stats = dict()

        for key in initial.keys() & stat_mapping.keys():
            self._stats[key.upper()] = stat_mapping[key](
                parent=self, initial=initial[key]
            )

        for key in stat_mapping.keys() - self._stats.keys():
            self._stats[key.upper()] = stat_mapping[key](parent=self, initial=0)

    def __getattr__(self, attr):
        return self._stats[attr]

    def __getitem__(self, attr):
        return self._stats[attr]
