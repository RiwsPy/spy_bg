import os
import warnings
from functools import cached_property
from typing import Self

from files import EntityFileReader, ids
from settings import GAME_VERSION
from utils.enums import TargetChoices


def create_entity(model, filename: str, **kwargs):
    return model(
        filename=filename.upper(),
        **(EntityFileReader.read(filename, model.fields) | kwargs),
    )


def create_entity_from_path(model, path: str, **kwargs):
    filename = path.rsplit(os.sep, 1)[-1].upper()
    return model(
        filename=filename,
        **(EntityFileReader.read(path, model.fields, is_path=True) | kwargs),
    )


class BgBase:
    ext: str = ""
    fields: dict = dict()
    filename: str = ""
    data_initial: dict = dict()
    expected_signature: str = ""

    def __init__(self, *args, **kwargs):
        for key in kwargs.keys() - self.fields.keys():
            setattr(self, key, kwargs.pop(key))

        self.data_initial = kwargs

        if self.expected_signature and self.expected_signature != self.signature:
            warnings.warn(
                f"{self.filename}: Incorrect signature: current : '{self.signature}' ; expected : '{self.expected_signature}'"
            )

    def __getattr__(self, attr: str):
        try:
            return self.data_initial[attr]
        except KeyError:
            if attr in self.fields:
                try:
                    return self.fields[attr].klass()
                except TypeError:
                    return None
            raise AttributeError(attr)

    def copy(self, **kwargs) -> Self:
        return self.__class__(**(self.__dict__ | kwargs))

    @classmethod
    def create_from_filename(cls, filename: str, **kwargs) -> Self:
        if cls.ext and "." not in filename:
            filename += cls.ext

        return create_entity(cls, filename, **kwargs)

    @classmethod
    def create_from_path(cls, path: str, **kwargs) -> Self:
        return create_entity_from_path(cls, path, **kwargs)

    @classmethod
    def get_tree_size(cls) -> int:
        return sum(i.size for i in cls.fields.values())


class HeaderBase(BgBase):
    @cached_property
    def effects(self) -> tuple:
        return tuple(eff for eff in self._effects if eff.opcode.is_used)

    @property
    def target_is_self(self) -> bool:
        return True

    @property
    def effects_not_instant(self) -> tuple:
        # opcodes utiles et ayant une durée
        return tuple(eff for eff in self.effects if not eff.opcode.is_instant)

    @property
    def projectile_id(self) -> int:
        return self._projectile - int(GAME_VERSION.startswith("BG"))

    @cached_property
    def range(self) -> int:
        if self.target == TargetChoices.PORTRAIT:
            return -1
        if self._range <= 1 or self.target_is_self:
            return 0
        return self._range

    @cached_property
    def projectile(self):
        from .projectile import Projectile

        projectile_name = ids.PROJECTL.get(self.projectile_id)
        projectile_kwargs = {"id": self.projectile_id}
        if projectile_name:
            pro = Projectile.create_from_filename(projectile_name, **projectile_kwargs)
        else:
            pro = Projectile(**projectile_kwargs)

        return pro
