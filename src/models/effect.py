from functools import cached_property
from typing import Self

from utils import (
    BaseType,
    Byte,
    Char,
    Charaway,
    Dword,
    Resref,
    Word,
)
from utils.enums import TimingModes

from .common import BgBase
from .opcode import OpcodeBase


class EffectMixin:
    ext = ".eff"
    fields = dict()
    parent_resource_type = 0
    parent_resource = ""
    caster_level = 1

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_opcode(kwargs["opcode_number"])

    @cached_property
    def timing_mode(self) -> TimingModes:
        return TimingModes(self._timing_mode)

    @property
    def opcode_number(self) -> int:
        return self.opcode.opcode_number

    def set_opcode(self, opcode_number: int) -> None:
        opcode_class = OpcodeBase.from_int(opcode_number)
        self.opcode = opcode_class(effect=self)

    @cached_property
    def probability(self) -> float:
        return self.opcode.probability

    @property
    def is_dissipable(self) -> bool:
        return self.opcode.is_dissipable

    @property
    def is_resist_magic(self) -> bool:
        return self.opcode.is_resist_magic

    def apply(self, *args, **kwargs) -> None:
        return self.opcode.apply(*args, **kwargs)

    @classmethod
    def create_default(cls, **kwargs) -> Self:
        default_values = {
            fieldname: fieldtype.klass() for fieldname, fieldtype in cls.fields.items()
        }

        return cls(**(default_values | kwargs))

    def get_zone_desc(self) -> str:
        return self.opcode.get_zone_desc()

    def get_zone_restriction(self) -> str:
        return self.opcode.get_zone_restriction()

    @cached_property
    def parent(self):
        from src.models import Item, Spell

        if self.parent_resource:
            match self.parent_resource_type:
                case 1:
                    Spell.create_from_filename(self.parent_resource)
                case 2:
                    Item.create_from_filename(self.parent_resource)
        return None


class EffectV1(EffectMixin, BgBase):
    fields = {
        "opcode_number": Word(),
        "target_type": Byte(),
        "power": Byte(),
        "p1": Dword(),
        "p2": Dword(),
        "_timing_mode": Byte(),
        "dispel_and_resistance": Byte(),
        "duration": Dword(),
        "probability_end": Byte(),  # end est avant start, ceci n'est pas une erreur
        "probability_start": Byte(),
        "resource1": Resref(),
        "_lvl_max": Dword(),
        "_lvl_min": Dword(),
        "saving_throw": Dword(),
        "saving_throw_bonus": Dword(signed=True),
        "special": Dword(signed=True),
    }
    p3 = p4 = p5 = 0
    resource2 = resource3 = ""
    primary_school = 0
    secondary_type = ""
    is_v2 = False

    @property
    def level_min(self) -> int:
        if self.opcode.have_dice:
            return 0
        return self._lvl_min

    @property
    def level_max(self) -> int:
        if self.opcode.have_dice:
            return 0
        return self._lvl_max

    @property
    def dice_thrown(self) -> int:
        if self.opcode.have_dice:
            return self._lvl_max
        return 0

    @property
    def dice_sides(self) -> int:
        if self.opcode.have_dice:
            return self._lvl_min
        return 0


fields_V2_base = {
    "signature": Char(),
    "version": Char(),
    "truc": Char(size=8, klass=None),
    # "truc2": Char,
    "opcode_number": Dword(),
    "target_type": Dword(),
    "power": Dword(),
    "p1": Dword(),
    "p2": Dword(),
    "_timing_mode": Word(),
    "timing2": Word(),
    "duration": Dword(),
    "probability_end": Word(),  # end est avant start, ceci n'est pas une erreur
    "probability_start": Word(),
    "resource1": Resref(),
    "dice_thrown": Dword(),
    "dice_sides": Dword(),
    "saving_throw": Dword(),
    "saving_throw_bonus": Dword(signed=True),
    "special": Dword(signed=True),
    "primary_school": Dword(),
    "unknown": Dword(klass=None),
    "level_min": Dword(),
    "level_max": Dword(),
    "dispel_and_resistance": Dword(),
    "p3": Dword(),
    "p4": Dword(),
    "p5": Dword(),
    "time_applied": Dword(),
    "resource2": Resref(),
    "resource3": Resref(),
    "coordinates": Dword(size=16, klass=None),
    # "caster_x": Dword(klass=None),
    # "caster_y": Dword(klass=None),
    # "target_x": Dword(klass=None),
    # "target_y": Dword(klass=None),
    "parent_resource_type": Dword(),  # 0, 1 (spell) ou 2 (item)
    "parent_resource": Resref(),  # Spell
    "parent_resource_flags": Dword(),  # hostile & co
    "projectile": Dword(),
    "parent_resource_slot": Dword(),  # emplacement inventaire
    "variable_name": Charaway(klass=None),
    "caster_level": Dword(),
    "first_apply": Dword(klass=None),
    "secondary_type": Dword(),
    "unknown2": BaseType(klass=None, size=4 * 15),
}

fields_V2_mini = fields_V2_base.copy()
del fields_V2_mini["signature"]
del fields_V2_mini["version"]


class EffectV2Min(EffectMixin, BgBase):
    fields = fields_V2_mini
    is_v2 = True


class EffectV2(EffectV2Min):
    fields = fields_V2_base
    expected_signature = "EFF "
