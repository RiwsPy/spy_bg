import math
from collections import namedtuple
from functools import cached_property
from typing import Self

from utils import (
    BaseType,
    Byte,
    Char,
    Dword,
    Resref,
    SchoolChoices,
    SpellTypeChoices,
    StrrefPK,
    Word,
)
from utils.common import distance_str, extract_single
from utils.enums import (
    Duration,
    JSChoices,
    JSImpactDisplay,
    TargetType,
    TimingModes,
    ZoneRestrictionSuffix,
)
from utils.timing import Timing

from .common import BgBase, HeaderBase, TargetChoices
from .effect import EffectV1

LevelDuration = namedtuple("levelDuration", ["level", "duration"])


class Header(HeaderBase):
    fields = {
        "form": Word(),
        "location": Word(),
        "icon": Resref(),
        "_target": Byte(),
        "target_count": Byte(),
        "_range": Word(),
        "level_min": Word(),
        "_casting_time": Word(),
        "times_per_day": Word(),
        "unused": BaseType(size=8, klass=None),
        "feature_count": Word(),
        "feature_offset": Word(),
        "unused2": Dword(klass=None),
        "_projectile": Word(),
    }

    @cached_property
    def target(self):
        return TargetChoices(self._target)

    @cached_property
    def casting_time(self) -> int:
        return self._casting_time

    @property
    def target_is_self(self) -> bool:
        return super().target_is_self and (
            self.target.is_self
            # NOTE: les convocations sont un contre-exemple
            # or self.target == TargetChoices.ZONE
            # and not self.projectile.is_area
        )

    @property
    def target_is_creature(self) -> bool:
        return self.target.is_creature

    def get_duration_desc(self) -> str:
        projectile = self.projectile
        durations = {eff.opcode.timing.get_desc() for eff in self.effects_not_instant}
        for eff in self.effects:
            for child in eff.opcode.children:
                if not isinstance(child, Spell):
                    continue
                durations.add(child.get_duration_desc())

        # TODO: prévoir une ligne à part pour le projectile
        if projectile and (pro_duration := projectile.get_duration_desc()):
            durations.add(pro_duration)

        if not durations:
            return Duration.INSTANT.value
        return extract_single(durations, f"{Duration.SPECIAL.value} : {durations}")

    def get_zone_restriction(self) -> str:
        if not self.effects:
            return TargetType.NONE

        restrictions = {
            zone_restriction
            for eff in self.effects
            if (zone_restriction := eff.get_zone_restriction())
        }
        return extract_single(restrictions, TargetType.NONE)

    def get_zone_desc(self) -> str:
        if not self.effects:
            return TargetType.NONE.value

        pro_restr = self.projectile.get_zone_restriction()
        # TODO: les effets peuvent avoir des sorts qui ont un projectile
        pro_zone = self.projectile.get_zone_desc()

        restrs = {
            zone_restr
            for obj in self.effects
            if (zone_restr := obj.get_zone_restriction())
        }

        if pro_zone:
            restrs = {eff_restr.plural for eff_restr in restrs}
            pro_restr = pro_restr.plural
        else:
            restrs = {eff_restr.value for eff_restr in restrs}
            pro_restr = pro_restr.value

        if pro_restr:
            restrs = {f"{eff_restr} {pro_restr}" for eff_restr in restrs} or {pro_restr}
        restrictions = " ou ".join(sorted(restrs))

        if not restrictions and not pro_zone:
            if self.target_is_self:
                restrictions = TargetType.SELF.value
            elif self.target == TargetChoices.PARTY:
                restrictions = TargetType.PARTY.value
            else:
                restrictions = TargetType.CREATURE.value

        prefix = ""
        if restrictions and restrictions not in (
            TargetType.SPECIAL,
            TargetType.SPECIAL.plural,
        ):
            pro_zone = pro_zone.plural
            if pro_zone:
                prefix = "les"
            elif self.target_is_self or self.target == TargetChoices.PARTY:
                prefix = "le"
            else:
                prefix = "1"
        else:
            pro_zone = pro_zone.value

        if pro_zone:
            pro_zone = pro_zone.format(
                cone_shape=self.projectile.get_cone_shape(),
                radius_desc=self.projectile.get_radius_desc(),
            )

        suffix = ""
        if not self.projectile.is_scorcher and self.projectile.is_area:
            if self.target_is_self:
                suffix = ZoneRestrictionSuffix.AROUND_SELF.value
            elif self.target_is_creature:
                suffix = ZoneRestrictionSuffix.AROUND_TARGET.value

        # FIXME: à inclure dans opcode
        first_effect = self.effects[0]
        if first_effect.opcode_number in (318, 324) and first_effect.p2 == 43:
            if suffix:
                suffix += " "
            suffix += ZoneRestrictionSuffix.EXCEPT_SELF.value

        return " ".join(
            part for part in (prefix, restrictions, pro_zone, suffix) if part
        )


class Spell(BgBase):
    ext = ".spl"
    expected_signature = "SPL "
    fields = {
        "signature": Char(),
        "version": Char(),
        "name": StrrefPK(),
        "iname": StrrefPK(klass=None),
        "sound": Resref(),
        "flags": Dword(),
        "_type": Word(),
        "usability": Dword(),
        "animation": Word(),
        "level_min": Byte(),
        "_school": Byte(),
        "strength": Byte(klass=None),
        "secondary_type": Byte(),
        "min_stat": Dword(klass=None, size=12),
        "level": Dword(),
        "stack_amount": Word(klass=None),
        "inventory_icon": Resref(),
        "lore": Word(klass=None),
        "ground_icon": Resref(klass=None),
        "weight": Dword(klass=None),
        "description": StrrefPK(),
        "idescription": StrrefPK(klass=None),
        "description_icon": Resref(klass=None),
        "enchantment": Dword(klass=None),
        "extended_offset": Dword(),
        "extended_count": Word(),
        "blocks_offset": Dword(),
        "casting_offset": Word(),
        "casting_count": Word(),
        "headers": BaseType(
            klass=list,
            model=Header,
            nb_key="extended_count",
            sub=BaseType(
                klass=list, model=EffectV1, nb_key="feature_count", name="_effects"
            ),
        ),
        "casting_features": BaseType(
            klass=list, model=EffectV1, nb_key="casting_count"
        ),
    }

    # influe la durée d'incantation, exclue la gestion de casting_features…
    casting_is_instant: bool = False

    def from_source(self, source) -> Self:
        return self.from_level(self.get_caster_level(source))

    def from_level(self, level: int) -> Self:
        """
        Renvoie une instance `Spell` en limitant ses headers à celui utilisable par le lanceur de sort
        """
        if len(self.headers) <= 1:
            return self

        header_index = 0
        for index, header in enumerate(self.headers):
            if header.level_min > level:
                header_index = max(0, index - 1)
                break

        return self.copy(headers=self.headers[header_index : header_index + 1])

    @property
    def effects_iter(self):
        for header in self.headers:
            yield from header.effects

    @cached_property
    def effects(self) -> tuple:
        return tuple(eff for eff in self.effects_iter)

    @property
    def effects_js(self) -> tuple:
        return tuple(
            eff for eff in self.effects_iter if not eff.opcode.is_can_be_instant
        )

    def get_saving_throws_desc(self) -> str | None:
        """
        Description des jets de sauvegarde
        """
        effects = self.effects_js

        # si or et and sont différents ⇒ présence d'effets avec JS et d'autres non
        saving_throws_or = JSChoices.NONE
        saving_throws_and = JSChoices.ALL
        for eff in effects:
            saving_throws_or |= eff.saving_throw & JSChoices.ALL
            saving_throws_and &= eff.saving_throw

        # FIXME: certains opcodes (214,) peuvent avoir un jet de sauvegarde spécifique
        if saving_throws_or == JSChoices.NONE:
            return JSChoices.NONE.label

        st_label = JSChoices.SPECIAL.label
        st_mod = ""
        strref = _("{st_label} ({st_impact})")
        # log_js % 1 == 0, si un seul js est en cours
        if (
            saving_throws_or == saving_throws_and
            and math.log(saving_throws_or, 2) % 1 == 0
        ):
            opcode_numbers = {eff.opcode_number for eff in effects}
            st_label = JSChoices(saving_throws_or).label_min(opcode_numbers)

            if st_mod := self.get_saving_throws_bonus_desc():
                if st_mod == JSImpactDisplay.SPECIAL.value:
                    strref = _("{st_label}, {st_mod} ({st_impact})")
                else:
                    strref = _("{st_label} {st_mod} ({st_impact})")

        # impact du js
        st_impacts = {eff.opcode.get_saving_throws_desc() for eff in effects}
        if not st_impacts:
            st_impact = JSImpactDisplay.NEGATE.value
        else:
            st_impact = extract_single(st_impacts, JSImpactDisplay.SPECIAL.value)

        if (
            st_label == JSChoices.SPECIAL.label
            and st_impact == JSImpactDisplay.SPECIAL.value
        ):
            return st_impact

        return strref.format(st_label=st_label, st_mod=st_mod, st_impact=st_impact)

    def get_saving_throws_bonus_desc(self) -> str:
        """
        Description des modificateurs de jets de sauvegarde
        """
        effects = self.effects_js
        if not effects:
            return JSImpactDisplay.NONE.value

        saving_throw_bonus = set(eff.saving_throw_bonus for eff in effects)
        value = extract_single(saving_throw_bonus)
        if value is not None:
            strref = "" if value == 0 else _("à {st_mod}")
            st_mod = (value > 0) * "+" + str(value)
            return strref.format(st_mod=st_mod)
        return JSImpactDisplay.SPECIAL.value

    @property
    def school(self) -> int:
        return SchoolChoices(self._school)

    @property
    def type(self) -> int:
        return SpellTypeChoices(self._type)

    @property
    def casting_time(self) -> int:
        if self.casting_is_instant:
            return 0
        return max(header.casting_time for header in self.headers)

    @property
    def range(self) -> float:
        return max(header.range for header in self.headers)

    def get_range_desc(self) -> str:
        """
        Description de la portée
        """
        # FIXME: opcode 212 vise la zone
        range_ = self.range
        if range_ == 0:
            if self.target_is_creature and not self.target_is_self:
                return _("contact")
            return _("0")
        elif range_ == -1:
            return _("illimitée")

        return distance_str(range_)

    @property
    def target_is_self(self) -> bool:
        return all(header.target_is_self for header in self.headers)

    @property
    def target_is_creature(self) -> bool:
        return all(header.target_is_creature for header in self.headers)

    def get_zone_desc(self) -> str:
        # TODO
        desc = {header.get_zone_desc() for header in self.headers}
        return extract_single(desc, _("spéciale"))

    def get_duration_desc(self) -> str:
        # TODO: calcul par niveau
        durations = [header.get_duration_desc() for header in self.headers]
        if len(set(durations)) == 1:
            return durations[0]
        # FIXME: perte d'informations (cas nuage puant, on a toutes les données et on affiche spéciale)
        if any(Duration.SPECIAL.value in duration for duration in durations):
            return Duration.SPECIAL.value

        duration_by_level = []
        for header in self.headers:
            durations = {eff.duration for eff in header.effects_not_instant}
            duration = extract_single(durations)
            if duration is None:
                break
            duration_by_level.append(LevelDuration(header.level_min, duration))
        else:
            # FIXME: le dernier niveau n'est pas nécessairement le plus long
            # time_max = Timing(TimingModes.INSTANT, duration_by_level[-1].duration).get_desc()
            # end = f"(jusqu'à {time_max})"
            end = ""
            if len(duration_by_level) >= 3:
                duration_diff = (
                    duration_by_level[2].duration - duration_by_level[1].duration
                )
                level_diff = duration_by_level[2].level - duration_by_level[1].level
                gain_by_level = duration_diff / level_diff
                base = (
                    duration_by_level[1].duration
                    - gain_by_level * duration_by_level[1].level
                )
                for level, duration in duration_by_level[1:]:
                    if base + level * gain_by_level != duration:
                        break
                else:
                    time_base = Timing(TimingModes.INSTANT, base).get_desc()
                    # time_max = Timing(TimingModes.INSTANT, duration).get_desc()
                    duration = ""
                    if time_base not in (Duration.NONE.value, Duration.INSTANT.value):
                        duration = time_base + " + "

                    if level_diff > 1:
                        end = _("par tranche de {level_diff} niveaux").format(
                            level_diff=level_diff
                        )
                    else:
                        end = _("par niveau")
                    # TODO: il serait intéressant de converser la même unité que le timing précédent
                    # end += f" (jusqu'à {time_max})"

                    duration += Timing(
                        TimingModes.INSTANT, gain_by_level * level_diff
                    ).get_desc()
                    return f"{duration} {end}"
                return _("variable selon niveau : non régulier ? {end}").format(end=end)
            return _("variable selon niveau : manque d'info ? {end}").format(end=end)
        return _("variable selon niveau")

    @property
    def is_dissipable(self) -> bool | None:
        values = {
            eff.is_dissipable
            for eff in self.effects_iter
            if eff.opcode.is_used and not eff.opcode.is_can_be_instant
        }
        return extract_single(values)

    @property
    def is_resist_magic(self) -> bool | None:
        values = {
            eff.is_resist_magic
            for eff in self.effects_iter
            if eff.opcode.is_used and not eff.opcode.is_can_be_instant
        }
        return extract_single(values)

    @property
    def is_hostile(self) -> bool:
        return self.flags & 0x400

    def get_caster_level(self, caster) -> int:
        if self.type == SpellTypeChoices.WIZARD:
            return caster.wizard_level
        elif self.type == SpellTypeChoices.PRIEST:
            return caster.level_priest
        return caster.level_average
