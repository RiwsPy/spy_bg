import gettext
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent.parent

# Chemin relatif pour accéder au répertoire du jeu
GAME_DIR = "mon/repo/BG/TODO/"

# nom du dossier contentant les fichiers du jeu
OVERRIDE_DIR = "override"

# Version du jeu
GAME_VERSION = "BG2EE"

# Mod nightmare actif
NIGHTMARE_MODE_ACTIVE = False

# Langue du jeu
LANGUAGE = "fr_FR"

TRA_FILES = {
    "game_version": {"language": str(BASE_DIR.parent) + "/tra/dialog.tra"},
    # Exemple :
    # "BG2EE": {"fr_FR": str(BASE_DIR.parent) + "/tra/dialog.tra"},
}

# meter is using if == True, foot instead
DISTANCE_UNIT_METER = True

# caractère de séparation entre les entiers et les décimaux
DECIMAL_COMMA = ","

t = gettext.translation("spy_bg", localedir="src/locales", languages=[LANGUAGE])
t.install()
_ = t.gettext
