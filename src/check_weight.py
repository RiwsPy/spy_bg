#!/usr/bin/python
import re

from common import Command as CommandBase
from models.item import Item


class Command(CommandBase):
    def get_queryset(self) -> list:
        def is_valid(itm):
            return (
                itm.description
                and itm.description.entry > 0
                or itm.udescription
                and itm.udescription.entry > 0
            )

        return [
            itm
            for file in self.files
            if file.endswith(".itm")
            and is_valid(itm := Item.create_from_filename(file))
        ]

    def handle(self, *args, **kwargs) -> None:
        super().handle(*args, **kwargs)
        regex_weight = re.compile(r"\nPoids : (?P<weight>\w+)")

        def check_weight(item) -> None:
            description = (
                item.description
                if item.description and item.description.entry > 0
                else item.udescription
            )
            match_weight = regex_weight.search(description)
            key = f"{item.filename} ⇒ Strref {description.entry}"
            if match_weight is None:
                if item.weight == 0:
                    return
                self.filename_info[key].append(f"Poids manquant : {item.weight}")
            else:
                desc_weight = int(match_weight.group("weight"))
                if desc_weight != item.weight:
                    self.filename_info[key].append(
                        f"Poids erroné : {desc_weight} ⇒ {item.weight}"
                    )

        for item in self.get_queryset():
            check_weight(item)


if __name__ == "__main__":
    Command()
