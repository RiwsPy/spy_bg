import os
import sys
import time
from collections import defaultdict

from files import GameFiles
from settings.dev import GAME_VERSION


class Command:
    help = ""
    output_dir = f"src{os.sep}logs"
    output_filename = ""
    _queryset = None

    def __init__(self):
        print("Starting…")
        self.time_start = time.time()
        self.files = GameFiles()
        self.filename_info = defaultdict(list)
        self._queryset = self.get_queryset()
        self.handle()
        self.output()

    def get_output_filename(self) -> str:
        default_filemame = sys.argv[0].rsplit(os.sep, 1)[-1].removesuffix(".py")
        return self.output_filename or f"{GAME_VERSION}_{default_filemame}.log"

    def get_queryset(self):
        raise NotImplementedError

    def handle(self, *args, **kwargs) -> None:
        pass

    def output(self) -> None:
        dir_name = os.path.join(self.output_dir, self.get_output_filename())
        with open(dir_name, "w+") as f:
            txt = ""
            for filename, errors in sorted(self.filename_info.items()):
                txt += f"{filename}\n\t" + "\n\t".join(errors) + "\n"
            f.write(txt)
        print(f"Writing in {dir_name}")
        print(time.time() - self.time_start)
