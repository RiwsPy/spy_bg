import warnings

from .common import GameFiles

__all__ = ["IdsReader"]


class IdsReader:
    @classmethod
    def read(cls, filename: str) -> dict[int, str]:
        def convert_entry(entry) -> int | None:
            try:
                return int(entry, 0)
            except ValueError:
                return None

        path = GameFiles.file_path(filename)
        datas = dict()
        with open(path, "r") as f:
            for line in f:
                entry, _, data = line.strip().partition(" ")
                if data and (int_entry := convert_entry(entry)) is not None:
                    datas[int_entry] = data

        if not datas:
            warnings.warn(f"File {filename} is empty")
        return datas


_IDS = dict()


def __getattr__(attr: str):
    if not attr.startswith("__"):
        attr = attr.upper()
        if attr not in _IDS:
            _IDS[attr] = IdsReader.read(f"{attr}.ids")
        return _IDS[attr]
    raise AttributeError
