import re
import warnings
from collections import namedtuple

from .common import GameFiles

__all__ = ["DaReader"]


regex_space = re.compile(r"[ \t]+")


class DaReader:
    @classmethod
    def read(cls, filename: str) -> dict[int, str]:
        def convert_entry(line) -> list:
            return regex_space.split(line.strip())

        datas = dict()

        try:
            path = GameFiles.file_path(filename)
        except FileNotFoundError:
            return datas

        with open(path, "r") as f:
            default_value = "-1"
            try:
                # skip de la première ligne: 2da version
                next(f)
                # default_value de la seconde ligne
                default_value = convert_entry(next(f))[0]
                # noms des colonnes de la troisième ligne
                field_names = convert_entry(next(f))
            except StopIteration:
                return datas

            # le rename permet de lire des fichiers 2da qui auraient des noms de colonnes commençant par un entier
            # mais ces colonnes ne sont pas accessibles par leur nom mais par le numéro '_24' pour la 25ème colonne
            col_type = namedtuple(
                "NamedTuple",
                field_names,
                rename=True,
                defaults=(default_value,) * len(field_names),
            )

            for line in f:
                key, *values = convert_entry(line)

                try:
                    line_values = col_type(*values)
                except TypeError:
                    line_values = col_type(values)

                datas[key] = line_values

        if not datas:
            warnings.warn(f"File {filename} is empty")
        return datas


_DA = dict()


def __getattr__(attr: str):
    if not attr.startswith("__"):
        attr = attr.upper()
        if attr not in _DA:
            _DA[attr] = DaReader.read(f"{attr}.2da")
        return _DA[attr]
    raise AttributeError
