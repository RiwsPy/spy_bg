import os
from typing import Self

from settings import GAME_DIR, GAME_VERSION, OVERRIDE_DIR


class GameFiles:
    game: str = ""
    _instance = None
    _data: dict[str, str] = dict()

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, *args, **kwargs):
        game = kwargs.pop("game", GAME_VERSION)
        super().__init__(*args, **kwargs)
        if game != self.game:
            self.load(game)
            self.game = game

    @classmethod
    def get_instance(cls, *args, **kwargs) -> Self:
        return cls(*args, **kwargs)

    @classmethod
    def load(cls, game_version: str, override_dir: str = OVERRIDE_DIR) -> None:
        path = os.path.join(GAME_DIR, game_version, override_dir)
        with os.scandir(path) as it:
            cls._data = {
                entry.name.lower(): entry.path for entry in it if entry.is_file()
            }

    @classmethod
    def file_path(cls, filename: str) -> str:
        try:
            return cls.get_instance()[filename]
        except KeyError:
            raise FileNotFoundError(f"{filename} not found.")

    def __getitem__(self, attr):
        return self._data[attr.lower()]

    def __contains__(self, attr):
        return attr in self._data

    def __iter__(self):
        return iter(self._data)


class EntityFileReader:
    @classmethod
    def read(cls, filename: str, fields: dict, is_path: bool = False, **kwargs) -> dict:
        if is_path:
            path = filename
        else:
            try:
                path = GameFiles.file_path(filename)
            except FileNotFoundError:
                return dict()

        with open(path, "rb") as f:
            return cls.read_offsets(f, fields, **kwargs)

    @classmethod
    def read_offsets(cls, f, fields: dict) -> dict:
        def create_instance(field, f):
            return field.model(**cls.read_offsets(f, field.model.fields))

        data = dict()
        sub_instances = list()
        for key, field in fields.items():
            value = None
            if field.size:
                offset = f.read(field.size)
                if field.klass is not None:
                    value = field.read_offset(offset)
            elif field.model:
                count = field.get_count(data)
                key = field.name or key
                value = tuple(create_instance(field, f) for _ in range(count))
                if field.sub:
                    sub_instances += [(instance, field.sub) for instance in value]

            data[key] = value

        for instance, field in sub_instances:
            count = field.get_count(instance.data_initial)
            value = tuple(create_instance(field, f) for _ in range(count))
            setattr(instance, field.name, value)
        return data
