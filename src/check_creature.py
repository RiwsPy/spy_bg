#!/usr/bin/python

from models.creature import Creature
from models.spell import Spell
from utils.file import GameFiles

if __name__ == "__main__":
    import time

    print("starting")
    start = time.time()

    files = GameFiles()
    cre = Creature.create_from_filename("NEERA10")
    spl = Spell.create_from_filename("SPWI304")
    cre.load()
    percent = spl.headers[0].effects[0].opcode._apply_percent(cre)

    print("finished")
    print(time.time() - start)
