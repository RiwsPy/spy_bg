#!/usr/bin/python
from collections import defaultdict

# from models.creature import Creature
from files import GameFiles
from models.item import Item
from utils.strref import DialogBase

if __name__ == "__main__":
    import time

    print("starting")
    start = time.time()

    data = dict()
    for game in ("BGEE-SOD", "BG2EE"):
        qs = dict()
        files = GameFiles(game=game)
        DialogBase(game=game)
        for file, path in files._data.items():
            if not file.endswith(".itm"):
                continue
            itm = Item.create_from_path(path)
            if (
                itm.description
                and itm.description.entry > 0
                or itm.udescription
                and itm.udescription.entry > 0
            ) and (
                itm.name and itm.name.entry > 0 or itm.uname and itm.uname.entry > 0
            ):
                qs[file.lower()] = [
                    itm,
                    itm.name,
                    itm.uname,
                    itm.description,
                    itm.udescription,
                    itm.description.entry if itm.description is not None else 0,
                    itm.udescription.entry if itm.udescription is not None else 0,
                ]
        data[game] = qs

    common_items = data["BGEE-SOD"].keys() & data["BG2EE"].keys()
    errors = defaultdict(list)

    def diff_desc(v1, v2, entry1, entry2, desc_name=""):
        if v1 != v2 and v1 is not None and v2 is not None and entry1 and entry2:
            diff = len(v1) - len(v2)
            base = f"{entry1} ⇒ {entry2}"
            if diff:
                errors[filename].append(
                    f"{base} ; {desc_name} différente : {diff} caractères"
                )
            else:
                errors[filename].append(
                    f"{base} ; {desc_name} différente mais longueur identique"
                )

    def next_name(name: str) -> str:
        words = name.split()
        if words[0] in ("Le", "La", "Les"):
            words[1] = words[1].capitalize()
            for i, word in enumerate(words[2:]):
                if not word.startswith("d'") and words[i + 1] != "de":
                    words[i + 2] = word.lower()
        else:
            if words[0].startswith("L'"):
                words[0] = words[0][:3].upper() + words[0][3:].lower()
            else:
                words[0] = words[0].capitalize()
            for i, word in enumerate(words[1:]):
                if not word.startswith("d'") and words[i] != "de":
                    words[i + 1] = word.lower()
        return " ".join(words)

    for filename in common_items:
        ee_name, ee_uname, ee_description, ee_udescription, ee_entry, ee_uentry = data[
            "BGEE-SOD"
        ][filename][1:]
        e2_name, e2_uname, e2_description, e2_udescription, e2_entry, e2_uentry = data[
            "BG2EE"
        ][filename][1:]
        # if ee_entry > 0 and ee_name != next_name(ee_name):
        #     errors[filename].append(
        #         f"BG1 : Nom non conforme: {ee_name} ⇒ {next_name(ee_name)}"
        #     )
        # if e2_entry > 0 and e2_name != next_name(e2_name):
        #     errors[filename].append(
        #         f"BG2 : Nom non conforme: {e2_name} ⇒ {next_name(e2_name)}"
        #     )
        if ee_name != e2_name:
            errors[filename].append(f"Nom différent : {ee_name} ⇒ {e2_name}")
        if ee_uname != e2_uname:
            errors[filename].append(f"Nom différent 2 : {ee_uname} ⇒ {e2_uname}")
        diff_desc(ee_description, e2_description, ee_entry, e2_entry, "Description")
        diff_desc(
            ee_udescription, e2_udescription, ee_uentry, e2_uentry, "Description2"
        )

    with open("src/logs/BGXEE_items_diff.log", "w+") as f:
        txt = ""
        for filename, errors in sorted(errors.items()):
            txt += f"{filename}\n\t" + "\n\t".join(errors) + "\n"
        f.write(txt)

    print("finished")
    print(time.time() - start)
