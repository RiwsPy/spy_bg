from enum import IntEnum, IntFlag

from files import ids


class DmgtypeEnum(IntEnum):
    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.UNKNOWN


def create_ids_enum(name):
    ids_data = getattr(ids, name)
    reverse_ids_data = {v: k for k, v in ids_data.items()}

    match name:
        case "ITEMFLAG" | "STATE":
            return IntFlag(name, reverse_ids_data)
        case "DMGTYPE":
            return DmgtypeEnum(name, reverse_ids_data | {"UNKNOWN": -1})
        case _:
            return IntEnum(name, reverse_ids_data)


_IDS_ENUMS = dict()


def __getattr__(attr: str):
    if not attr.startswith("__"):
        attr = attr.upper()
        if attr not in _IDS_ENUMS:
            _IDS_ENUMS[attr] = create_ids_enum(attr)
        return _IDS_ENUMS[attr]
    raise AttributeError(attr)
