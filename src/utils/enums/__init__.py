from enum import Enum, IntEnum, IntFlag, unique
from functools import cached_property

from utils.common import PluralLabelEnum

from .creature import *  # noqa
from .ids_reverse import *  # noqa

st_stat_name = {
    "SAVEVSSPELL",
    "SAVEVSBREATH",
    "SAVEVSDEATH",
    "SAVEVSWANDS",
    "SAVEVSPOLY",
}


class IntLabelEnum(int, Enum):
    def __new__(cls, value: int, label: str):
        obj = int.__new__(cls, value)
        obj._value_ = value
        obj.label = label
        return obj


class JSChoices(IntFlag):
    NONE = 0x0, _("aucun")
    SPELL = 0x1, _("sorts")
    BREATH = 0x2, _("souffles")
    HOLD_DEATH_POISON = 0x4, _("paralysie, mort, poison")
    WAND = 0x8, _("baguettes")
    METAMORPHOSE_PETRIFICATION = 0x10, _("métamorphose, pétrification")
    ALL = 0x1F, _("tous")

    SPECIAL = 0xFF, _("spécial")

    def __new__(cls, value: int, label: str):
        obj = int.__new__(cls, value)
        obj._value_ = value
        obj.label = label
        return obj

    def label_min(self, opcode_numbers: set) -> str:
        label = self.label
        if self == self.HOLD_DEATH_POISON:
            if {12, 25, 78} & opcode_numbers:
                label = _("poison")
            elif {13, 39, 55, 209, 211, 216, 238} & opcode_numbers:
                label = _("mort")
            elif {45, 109, 154, 157, 175, 185} & opcode_numbers:
                label = _("paralysie")
        elif self == self.METAMORPHOSE_PETRIFICATION:
            if {111, 135} & opcode_numbers:
                label = _("métamorphose")
            elif {13, 109, 134, 175, 185} & opcode_numbers:
                label = _("pétrification")
        return label

    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.SPECIAL


class JSImpactDisplay(Enum):
    NONE = _("aucun")
    HALF = _("1/2")
    NEGATE = _("annule")
    PARTIAL = _("partiel")
    SPECIAL = _("spécial")


class TimeUnits(Enum):
    DAY = _("jour"), _("jours")
    HOUR = _("heure"), _("heures")
    TOUR = _("tour"), _("tours")
    ROUND = _("round"), _("rounds")
    SECOND = _("seconde"), _("secondes")
    TICK = _("tick"), _("ticks")

    @cached_property
    def tick_duration(self) -> int:
        return timeunit_to_tick_duration[self]


timeunit_to_tick_duration = {
    TimeUnits.DAY: 15 * 60 * 120,
    TimeUnits.HOUR: 15 * 60 * 5,
    TimeUnits.TOUR: 15 * 60,
    TimeUnits.ROUND: 15 * 6,
    TimeUnits.SECOND: 15,
    TimeUnits.TICK: 1,
}


class Duration(Enum):
    SPECIAL = _("spéciale")
    UNKNOWN = _("inconnue")
    INSTANT = _("instantanée")
    PERMANENT = _("permanente")
    NONE = _("0")
    TRIGGER = _("permanente jusqu'à déclenchement")


@unique
class TimingModes(IntEnum):
    INSTANT = 0
    PERMANENT = 1
    EQUIPED = 2
    DELAY_INSTANT = 3
    DELAY_PERMANENT = 4
    DELAY_EQUIPED = 5
    DELAY_INSTANT_ABS = 6
    DELAY_PERMANENT_ABS = 7
    DELAY_EQUIPED_ABS = 8
    REALLY_PERMANENT = 9
    INSTANT_ABS = 10
    TIME_ABS = 4096

    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.PERMANENT

    @cached_property
    def is_equiped(self) -> bool:
        return "EQUIPED" in self.name

    @cached_property
    def is_absolute(self) -> bool:
        return self.name.endswith("_ABS")

    @cached_property
    def is_delay(self) -> bool:
        return self.name.startswith("DELAY_")


class TargetChoices(IntEnum):
    UNKNOWN = 0
    CREATURE = 1
    INVENTORY = 2
    PORTRAIT = 3
    ZONE = 4
    SELF = 5
    PARTY = 6
    SELF_INSTANT = 7

    @property
    def is_self(self) -> bool:
        return self in {self.SELF, self.SELF_INSTANT}

    @property
    def is_creature(self) -> bool:
        return self in {self.SELF, self.SELF_INSTANT, self.CREATURE}

    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.UNKNOWN


@unique
class SchoolChoices(IntLabelEnum):
    ALL = 0, _("Toutes écoles")
    ABJURATION = 1, _("Abjuration")
    CONJURATION = 2, _("Conjuration")
    DIVINATION = 3, _("Divination")
    ENCHANTMENT = 4, _("Enchantement")
    ILLUSION = 5, _("Illusion")
    INVOCATION = 6, _("Invocation")
    NECROMANCY = 7, _("Nécromancie")
    TRANSMUTER = 8, _("Transmutation")
    GENERALIST = 9, _("Généraliste")
    WILDMAGE = 10, _("Entropie")
    UNKNOWN = 11, _("Inconnue")

    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.UNKNOWN

    @property
    def stat_name(self) -> str:
        return f"SAVE{self.name}_BONUS"


@unique
class KitChoices(IntEnum):
    UNKNOWN = 0x0
    WILDMAGE = 0x8000
    MAGESCHOOL_ABJURER = 0x00400000
    MAGESCHOOL_CONJURER = 0x00800000
    MAGESCHOOL_DIVINER = 0x01000000
    MAGESCHOOL_ENCHANTER = 0x02000000
    MAGESCHOOL_ILLUSIONIST = 0x04000000
    MAGESCHOOL_INVOKER = 0x08000000
    MAGESCHOOL_NECROMANCER = 0x10000000
    MAGESCHOOL_TRANSMUTER = 0x20000000
    MAGESCHOOL_GENERALIST = 0x40000000

    @property
    def mage_school(self) -> SchoolChoices:
        if self.value == self.WILDMAGE:
            return SchoolChoices.WILDMAGE
        return SchoolChoices(self.value / self.MAGESCHOOL_ABJURER)

    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.UNKNOWN


class SpellTypeChoices(IntLabelEnum):
    UNKNOWN = 0, _("?")
    WIZARD = 1, _("Profane")
    PRIEST = 2, _("Divin")
    PSIONIC = 3, _("Psionique")
    INNATE = 4, _("Innée")
    BARD_SONG = 5, _("Chant")

    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.UNKNOWN


class ISlot(IntEnum):
    INVALID = -1
    HELMET = 0
    ARMOR = 1
    SHIELD = 2
    GLOVES = 3
    L_RING = 4
    R_RING = 5
    AMULET = 6
    BELT = 7
    BOOTS = 8
    WEAPON_1 = 9
    WEAPON_2 = 10
    WEAPON_3 = 11
    WEAPON_4 = 12
    QUIVER_1 = 13
    QUIVER_2 = 14
    QUIVER_3 = 15
    QUIVER_4 = 16
    CLOAK = 17
    QUICK_ITEM_1 = 18
    QUICK_ITEM_2 = 19
    QUICK_ITEM_3 = 20
    INVENTORY_ITEM_1 = 21
    INVENTORY_ITEM_2 = 22
    INVENTORY_ITEM_3 = 23
    INVENTORY_ITEM_4 = 24
    INVENTORY_ITEM_5 = 25
    INVENTORY_ITEM_6 = 26
    INVENTORY_ITEM_7 = 27
    INVENTORY_ITEM_8 = 28
    INVENTORY_ITEM_9 = 29
    INVENTORY_ITEM_10 = 30
    INVENTORY_ITEM_11 = 31
    INVENTORY_ITEM_12 = 32
    INVENTORY_ITEM_13 = 33
    INVENTORY_ITEM_14 = 34
    INVENTORY_ITEM_15 = 35
    INVENTORY_ITEM_16 = 36
    MAGIC_WEAPON = 37
    SELECTED_WEAPON = 38
    SELECTED_WEAPON_ABILITY = 39

    @classmethod
    def _missing_(cls, *args, **kwargs):
        return cls.INVALID

    @classmethod
    def weapon_slots(cls) -> set:
        return set(cls(i) for i in range(cls.WEAPON_1, cls.WEAPON_4 + 1))

    @classmethod
    def quiver_slots(cls) -> set:
        return set(cls(i) for i in range(cls.QUIVER_1, cls.QUIVER_4 + 1))

    @classmethod
    def quick_item_slots(cls) -> set:
        return set(cls(i) for i in range(cls.QUICK_ITEM_1, cls.QUICK_ITEM_3 + 1))

    @classmethod
    def inventory_item_slots(cls) -> set:
        return set(
            cls(i) for i in range(cls.INVENTORY_ITEM_1, cls.INVENTORY_ITEM_16 + 1)
        )

    @classmethod
    def stuff_slots(cls) -> set:
        return set(cls(i) for i in range(cls.CLOAK + 1))


class TargetType(PluralLabelEnum):
    NONE = "", ""
    NO = _("aucune"), _("aucune")
    SPECIAL = _("spéciale"), _("spéciales")
    ALL = _("toutes"), _("toutes")

    SELF = _("lanceur"), _("lanceurs")
    PARTY = _("groupe"), _("groupes")

    NON_PARTY = _("créature hors groupe"), _("créatures hors groupe")
    ALLY = _("allié"), _("alliés")
    ENEMY = _("ennemi"), _("ennemis")
    CREATURE = _("créature"), _("créatures")
    ANY = _("cible inanimée"), _("cibles inanimées")
    FAMILAR = _("familier"), _("familiers")
    LOCK = _("porte ou coffre verrouillé(e)"), _("portes ou coffres verrouillé(e)s")

    RESTR = _("{restriction}"), _("{restriction}")


class ZoneDesc(PluralLabelEnum):
    NONE = "", ""
    CONE = (
        _("cône de {radius_desc} de long sur un arc de {cone_shape}°"),
        _("dans un cône de {radius_desc} de long sur un arc de {cone_shape}°"),
    )
    SCORCHER = (
        _("ligne de {radius_desc} de large"),
        _("sur une ligne de {radius_desc} de large"),
    )
    AREA = _("rayon de {radius_desc}"), _("dans un rayon de {radius_desc}")
    SPECIAL = _("spéciale"), _("dans une zone spéciale")


class ZoneRestrictionSuffix(Enum):
    AROUND_SELF = _("autour du lanceur")
    AROUND_TARGET = _("autour de la cible")
    EXCEPT_SELF = _("(autre que le lanceur)")
