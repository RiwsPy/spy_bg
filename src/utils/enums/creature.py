from utils.common import PluralLabelEnum

__all__ = ["TypeOrder"]

TypeOrder = (
    "",
    "",
    "EA",
    "GENERAL",
    "RACE",
    "CLASS",
    "SPECIFIC",
    "GENDER",
    "ALIGN",
    "KIT",
)


class TypeBase(PluralLabelEnum):
    NONE = "", ""


class EA(PluralLabelEnum):
    pass


class GENERAL(PluralLabelEnum):
    HUMANOID = _("humanoïde"), _("humanoïdes")
    ANIMAL = _("animal"), _("animaux")
    DEAD = _("mort"), _("morts")
    UNDEAD = _("mort-vivant"), _("morts-vivants")
    GIANTHUMANOID = _("géant"), _("géants")
    FROZEN = _("froid"), _("froid")
    PLANT = _("plante"), _("plantes")
    MONSTER = _("monstre"), _("monstres")

    DEFAULT = _("créature"), _("créatures")


class RACE(PluralLabelEnum):
    pass


class CLASS(PluralLabelEnum):
    pass


class SPECIFIC(PluralLabelEnum):
    pass


class GENDER(PluralLabelEnum):
    MALE = _("homme"), _("hommes")
    FEMALE = _("femme"), _("femmes")
    SUMMONED = _("créature invoquée"), _("créatures invoquées")
    SUMMONED_DEMON = _("démon invoqué"), _("démons invoqués")
    ILLUSIONARY = _("illusion"), _("illusions")
    IMPRISONED_SUMMONED = _("créature emprisonnée"), _("créatures emprisonnées")


class ALIGN(PluralLabelEnum):
    pass


class KIT(PluralLabelEnum):
    pass
