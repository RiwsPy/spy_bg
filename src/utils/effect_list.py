class EffectList(list):
    def filter(self, **kwargs):
        return self.__class__(
            elt
            for elt in self
            if all(getattr(elt, key) == value for key, value in kwargs.items())
        )

    def exlude(self, **kwargs):
        return self.__class__(
            elt
            for elt in self
            if any(getattr(elt, key) != value for key, value in kwargs.items())
        )

    def __add__(self, other):
        return self.__class__(super().__add__(other))

    def __getitem__(self, value):
        return self.__class__(super().__getitem__(value))
