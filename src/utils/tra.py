import warnings

from files import ids
from utils.enums import creature
from utils.enums.creature import TypeOrder


def translate_creature_type(file_entry: int | str, ctype: int | str) -> str:
    if isinstance(file_entry, int):
        file_entry = TypeOrder[file_entry]
    if isinstance(ctype, int):
        ctype = getattr(ids, file_entry)[ctype]

    file_entry = file_entry.upper()
    ctype = ctype.upper()

    try:
        return getattr(getattr(creature, file_entry), ctype)
    except AttributeError:
        warnings.warn(f"Unknown {file_entry} type: {ctype}")
    return creature.TypeBase.NONE
