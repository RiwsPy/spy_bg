from enum import Enum
from typing import Any, Iterable

from settings import DECIMAL_COMMA, DISTANCE_UNIT_METER

from .field import *  # noqa


class PluralLabelEnum(str, Enum):
    def __new__(cls, name: str, plural: str):
        obj = str.__new__(cls, name)
        obj._value_ = name
        obj.plural = plural
        return obj


class DistanceUnits(Enum):
    METER = _("mètre"), _("mètres")
    FOOT = _("pied"), _("pieds")


FEET_TO_METER = 0.3048


def pluralize(value, text: str, end: str = "s") -> str:
    return text + end * (value >= 2)


def pluralizes(value, texts: Iterable[str]) -> str:
    # texts est un iterable de deux éléments : (singulier, pluriel)
    return texts[value >= 2]


def distance_round(distance: int | float) -> int | float:
    # Méthode d'arrondissement des distances
    # Arrondi à 0.1 près si <= 4.5 ; à 0.5 près sinon
    if distance <= 4.5:
        return round(distance, 1)
    return round((distance + 0.25) // 0.5) * 0.5


def distance_str(distance: int | float) -> str:
    if DISTANCE_UNIT_METER:
        txts, mult = DistanceUnits.METER.value, FEET_TO_METER
    else:
        txts, mult = DistanceUnits.FOOT.value, 1

    value = distance_round(distance * mult)
    suffix = pluralizes(value, txts)

    value_str = str(value).removesuffix(".0").replace(".", DECIMAL_COMMA)
    return _("{value} {distance_unit}").format(value=value_str, distance_unit=suffix)


def extract_single(data: set, default=None) -> Any:
    """
    Renvoie l'unique valeur du set ou bien `default`
    """
    if len(data) == 1:
        return next(iter(data))
    return default
