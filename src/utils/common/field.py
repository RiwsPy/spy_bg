from typing import Self


class BaseType:
    # class ciblée pour convertir le contenu de la donnée
    # klass=None permet de lire sans l'interpréter (perf)
    klass = None

    # l'élément est converti en class `model`
    model = None

    # renseigne le nombre d'occurences de model (fixe)
    nb: int = 1

    # nom de la clé qui renseigne le nombre d'occurences de model (variable)
    nb_key: str = ""

    # nombre de bits occupés par la donnée
    size: int = 0

    # le model lui-même admet un type particulier
    sub = None

    # si ce type est particulier, il sera disponible dans `instance.name`
    name: str = ""
    """
        "headers": BaseType(
            klass=list,
            model=Header,
            nb_key="extended_count",
            sub=BaseType(
                klass=list, model=EffectV1, nb_key="feature_count", name="effects"
        ),
    """

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

        if self.model and self.size is None:
            self.size = self.model.get_tree_size()

    def copy(self) -> Self:
        return self.__class__(**self.__dict__)

    def get_count(self, value: dict) -> int:
        return value.get(self.nb_key, self.nb)

    def read_offset(self, offset):
        return


class BaseInt(BaseType):
    klass = int
    signed = False

    def read_offset(self, offset):
        return int.from_bytes(offset, "little", signed=self.signed)


class BaseStr(BaseType):
    klass = str

    def read_offset(self, offset):
        null_index = next((i for i, c in enumerate(offset) if c == 0), len(offset))
        value = offset[:null_index].decode("cp1252").rstrip("\x00")

        if value in ("", "None"):
            return None
        return value


class Byte(BaseInt):  # ~ -128 → 127
    size = 1


class Word(BaseInt):  # -32768 → 32767
    size = 2


class Dword(BaseInt):  # ~ -2MM → +2MM
    size = 4


class Char(BaseStr):  # ~ "1234"
    size = 4


class Resref(BaseStr):  # ~ "12345678"
    size = 8


class Charaway(BaseStr):
    size = 32
