import re

from settings import GAME_VERSION, LANGUAGE, TRA_FILES
from utils.common import BaseInt

regex_strref_data = re.compile(
    r"~([^~]*)~ ?\[?([A-Z0-9]*)]? ?~? ?([^~]*)?~? ?\[?([A-Z0-9]*)]?~?\n"
)


class DialogBase:
    _instance = None
    _entries: tuple = tuple()

    @classmethod
    def get_instance(cls, *args, **kwargs):
        return cls(*args, **kwargs)

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, game: str = ""):
        if game:
            self.load(game=game)
        elif not self._entries:
            self.load()

    def __getitem__(self, item):
        return self._entries[item]

    def __iter__(self):
        return iter(self._entries)

    def load(
        self, game: str = GAME_VERSION, language: str = LANGUAGE, **kwargs
    ) -> None:
        def data_to_strref(entry, text):
            data = regex_strref_data.findall(text)[0]
            value, sound, s, s_sound = data
            return Strref(value, entry=int(entry), s=s, sound=sound, s_sound=s_sound)

        with open(TRA_FILES[game][language]) as f:
            lines = re.split(r"^@(\d+) *= *", f.read(), flags=re.MULTILINE)
            self._entries = tuple(
                data_to_strref(entry, text)
                for entry, text in zip(lines[1::2], lines[2::2], strict=True)
            )

    @classmethod
    def get(cls, nb: int) -> "Strref":
        try:
            return cls.get_instance()[nb]
        except (KeyError, IndexError):
            return Strref()


class Strref(str):
    def __new__(cls, *args, entry=None, s=None, sound=None, s_sound=None):
        instance = super().__new__(cls, *args)
        instance.entry = entry
        instance.s = s
        instance.sound = sound
        instance.s_sound = s_sound
        return instance


class StrrefPK(BaseInt):
    size = 4
    model = Strref
    signed = True

    def read_offset(self, offset):
        value = super().read_offset(offset)

        return DialogBase.get(max(0, value))
