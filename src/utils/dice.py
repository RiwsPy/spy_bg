import random
import re

dice_regex = re.compile(
    r"^(?P<dice_count>\d+)[d|D](?P<dice_size>\d+)(?P<bonus>[\+|-]?\d*)$"
)


class Dice:
    def __init__(self, dice: tuple[int, int] | tuple[int, int, int], luck: int = 0):
        match dice:
            case (dice_count, dice_size, bonus):
                pass
            case (dice_count, dice_size):
                bonus = 0
            case _:
                raise ValueError("dice tuple len incorrect")

        self.dice_size = dice_size
        self.dice_count = dice_count
        self.bonus = bonus
        self.luck = luck
        self.roll_min = 1
        self.roll_max = self.dice_size
        if self.luck > 0:
            self.roll_min = min(dice_size, 1 + self.luck)
        elif self.luck < 0:
            self.roll_max = max(dice_size + self.luck, 1)

    def roll(self) -> float:
        return sum(
            random.randint(self.roll_min, self.roll_max) for _ in range(self.dice_count)
        ) + float(self.bonus)

    def average(self) -> float:
        return self.dice_count * (self.roll_min + self.roll_max) / 2 + float(self.bonus)
