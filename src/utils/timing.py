from functools import cached_property

from utils.enums import Duration, TimeUnits, TimingModes

from .common import pluralizes

timing_mapping = {
    TimingModes.INSTANT: TimingModes.INSTANT_ABS,
    TimingModes.DELAY_PERMANENT_ABS: TimingModes.PERMANENT,
    TimingModes.DELAY_PERMANENT: TimingModes.PERMANENT,
    TimingModes.DELAY_EQUIPED_ABS: TimingModes.EQUIPED,
    TimingModes.DELAY_EQUIPED: TimingModes.EQUIPED,
    TimingModes.DELAY_INSTANT_ABS: TimingModes.INSTANT_ABS,
    TimingModes.DELAY_INSTANT: TimingModes.INSTANT_ABS,
}


class TimingMixin:
    def __init__(self, timing_mode: TimingModes, duration: int):
        self.data_initial = {
            "timing_mode": timing_mode,
            "duration": duration,
        }

        # le temps est en seconde ⇒ convertion en tick
        if not timing_mode.is_absolute:
            duration *= TimeUnits.SECOND.tick_duration

        self.duration_delay = 0
        if timing_mode.is_delay:
            self.duration_delay = duration

        # les (0, 3, 6) ⇒ 10, (4, 7) ⇒ 1, (5, 8) ⇒ 2
        if timing_mode in timing_mapping:
            timing_mode = timing_mapping[timing_mode]

        self.duration = None
        if timing_mode == TimingModes.INSTANT_ABS:
            self.duration = duration
        elif timing_mode == TimingModes.TIME_ABS:
            self.duration = -1  # impossible à déterminer

        self.timing_mode = timing_mode

    def tick_to_time(self, value: int | None, timing_mode: int) -> str:
        if value is None or value >= 300000 * TimeUnits.SECOND.tick_duration:
            return Duration.PERMANENT.value
        elif value == 0:
            if timing_mode == TimingModes.INSTANT_ABS:
                return Duration.INSTANT.value
            return Duration.NONE.value
        elif value < 0:
            return Duration.SPECIAL.value
        elif value < TimeUnits.SECOND.tick_duration:
            return Duration.INSTANT.value

        for time_unit in TimeUnits._member_map_.values():
            duration = time_unit.tick_duration
            if value % duration == 0:
                unit_names = time_unit.value
                value = int(value / duration)
                break
        else:
            unit_names = TimeUnits.TICK.value

        return _("{value} {duration_unit}").format(
            value=value, duration_unit=pluralizes(value, unit_names)
        )

    def get_desc(self, value: str = "") -> str:
        if not value:
            value = self.tick_to_time(self.duration, self.timing_mode)
        if self.duration_delay:
            return _("{value}, après {delay_value}").format(
                value=value,
                delay_value=self.tick_to_time(self.duration_delay, self.timing_mode),
            )
        return value

    @cached_property
    def is_permanent(self) -> bool:
        return self.data_initial["timing_mode"] == TimingModes.PERMANENT

    @cached_property
    def is_permanent_after_death(self) -> bool:
        return self.data_initial["timing_mode"] == TimingModes.REALLY_PERMANENT


class Timing(TimingMixin):
    pass


class TimingOpcode(TimingMixin):
    def __init__(self, opcode):
        super().__init__(opcode.timing_mode, opcode.duration)
        self.opcode = opcode

    def tick_to_time(self, *args, **kwargs):
        if self.opcode.opcode_number in (234, 257):  # Contingence
            return Duration.TRIGGER.value
        elif self.opcode.is_permanent:
            return Duration.PERMANENT.value
        elif self.opcode.is_instant:
            return Duration.INSTANT.value
        return super().tick_to_time(*args, **kwargs)

    @cached_property
    def is_permanent_after_death(self) -> bool:
        return (
            super().is_permanent_after_death
            or not self.opcode.is_not_permanent_after_death
        )
