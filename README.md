##  Explorateur de fichier BG en python3.11+
Version légère, en cours de construction.\
Aucune dépendance externe.


## Fait ou fait pas

### Actuellement
- sert pour repérer les incohérences entre les cartouches des objets et des sorts à leurs effets réels

### A pour objectif de
- reconstituer les caractéristiques des personnages (celles de base + celles de l'équipement) afin d'analyser leurs faiblesses éventuelles

### N'a pas pour objectif de
- faire le café
- conquérir le monde
- être utilisé par d'autres personnes que son auteur. Mais libre à vous d'en faire ce que vous voulez


### Notes
- Ne lit pas `chtin.key`, actuellement les fichiers du jeu doivent être exportés en amont


### Utilisation
```sh
# pour lancer un script spécifique
./manage.py runscript /src/nom_du_script.py

# pour compiler les .po
./manage.py build_tra
```