#!/usr/bin/python

import argparse
import os
import subprocess
import sys

from setuptools.command.build import build
from setuptools.dist import Distribution

import polib

REQUIRED_MINIMUM_PYTHON_VERSION = (3, 11)

if sys.version_info < REQUIRED_MINIMUM_PYTHON_VERSION:
    error_message = f"spy_bg needs python >= {'.'.join(str(x) for x in REQUIRED_MINIMUM_PYTHON_VERSION)}\n"
    sys.stderr.write(error_message)
    sys.exit(1)

PO_FOLDER = os.path.join("src", "locales")


class BuildTransFiles(build):
    def run(self):
        po_filename = "spy_bg.po"
        mo_filename = "spy_bg.mo"
        for lang in os.listdir(PO_FOLDER):
            search_dir = os.path.join(PO_FOLDER, lang, "LC_MESSAGES")
            if not os.path.isdir(search_dir):
                continue

            if po_filename in os.listdir(search_dir):
                po_file = os.path.join(search_dir, po_filename)
                mo_file = os.path.join(search_dir, mo_filename)

                print("Compiling", po_file, "to", mo_file)
                po = polib.pofile(po_file)
                po.save_as_mofile(mo_file)


class RunScript(build):
    def run(self):
        script_path = sys.argv[2]
        script_abs_path = os.path.abspath(script_path)
        subprocess.run([sys.executable, script_abs_path] + sys.argv[2:])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script with multiple commands")
    subparsers = parser.add_subparsers(title="commands", dest="command")

    runscript_parser = subparsers.add_parser("runscript", help="Run a script")
    runscript_parser.add_argument("script_path", help="Path to the script")

    subparsers.add_parser("build_tra", help="Compile .po files to .mo files")

    args = parser.parse_args()
    distrib = Distribution({"name": "spy_bg", "version": "1.0"})  # TODO

    cmd_map = {
        "runscript": RunScript,
        "build_tra": BuildTransFiles,
    }
    cmd_map[args.command](distrib).run()
